from DataverseHelpers import DataverseImportBase
from FZJuelichMapping import FZJuelichMapping
from search_index_import_commons.Helpers import get


class FZJuelichImport(DataverseImportBase):

    def __init__(self):
        super().__init__("Forschungszentrum Jülich", "data.fz-juelich.de", mapping=FZJuelichMapping())

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        if "image" not in meta or not meta["image"]:
            meta["image"] = "https://data.fz-juelich.de/branding/files/dataverse/img/logo.png"
        meta["sourceOrganization"] = [{"type": "Organization", "name": "Forschungszentrum Jülich", "id": "https://ror.org/02nv7yv05"}]
        src_license = get(record["data"]["latestVersion"], "license")
        if src_license and self.mapping.get("license", src_license):
            meta["license"] = {"id": self.mapping.get("license", src_license)}
        return meta


if __name__ == "__main__":
    FZJuelichImport().process()
