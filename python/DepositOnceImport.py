import re

from DepositOnceMapping import DepositOnceMapping
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader


def build_creator(creator):
    if not creator:
        return None
    c_name = re.split(", ?", creator.strip())
    c_name.reverse()
    name = " ".join(c_name)
    return {"type": "Person", "name": name.strip()}


def get_identifier(record, namespaces):
    uris = record.findall("xoai:element[@name='identifier']/xoai:element[@name='uri']/xoai:element/xoai:field", namespaces)
    identifier = next((uri.text for uri in uris if "doi.org/" in uri.text), None)
    if not identifier:
        identifier = next((uri.text for uri in uris if "depositonce.tu-berlin.de/handle" in uri.text), None)
    return identifier


class DepositOnceImport(ResodateImporter):
    def __init__(self):
        super().__init__(mapping=DepositOnceMapping())
        self.oai = OaiPmhReader("https://api-depositonce.tu-berlin.de/server/oai/request", "xoai", None, self.user_agent)

    def get_name(self):
        return "DepositOnce"

    def load_next(self):
        return self.oai.load_next()

    def to_search_index_metadata(self, record):
        namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/", "xoai": "http://www.lyncode.com/xoai"}
        record_status = record.find("oai:header", namespaces)
        if "status" in record_status.attrib and record_status.get("status") == "deleted":
            return None
        dc_xml = record.find("oai:metadata/xoai:metadata/xoai:element[@name='dc']", namespaces)
        identifier = get_identifier(dc_xml, namespaces)
        if not identifier:
            return None
        keywords = list(set(map(lambda s: s.text.strip(), dc_xml.findall("xoai:element[@name='subject']/xoai:element[@name='other']/xoai:element/xoai:field", namespaces))))
        subjects = list(set(filter(None, map(lambda s: self.mapping.get("subject", s.text.strip()), filter(None, dc_xml.findall("xoai:element[@name='subject']/xoai:element[@name='ddc']/xoai:element/xoai:field", namespaces))))))
        language = dc_xml.findtext("xoai:element[@name='language']/xoai:element/xoai:element/xoai:field", None, namespaces)
        language_code = re.sub("_.*", "", language) if language else None
        description = dc_xml.findtext("xoai:element[@name='description']/xoai:element[@name='abstract']/xoai:element/xoai:field", None, namespaces)
        license_uri = dc_xml.findtext("xoai:element[@name='rights']/xoai:element[@name='uri']/xoai:element/xoai:field", None, namespaces)
        license_uri = self.mapping.get("license", license_uri) if license_uri else None
        creator_xml = dc_xml.findall("xoai:element[@name='contributor']/xoai:element[@name='author']/xoai:element/xoai:field[@name='value']", namespaces)
        contributor_xml = dc_xml.findall("xoai:element[@name='contributor']/xoai:element[@name='advisor']/xoai:element/xoai:field", namespaces)
        publisher_xml = dc_xml.findall("xoai:element[@name='publisher']/xoai:element/xoai:field", namespaces)
        lrts = list(set(map(lambda t: self.mapping.get("lrt", t.text.strip()),dc_xml.findall("xoai:element[@name='type']/xoai:element/xoai:field", namespaces))))
        bundles_xml = list(filter(lambda e: "ORIGINAL" == e.findtext("xoai:field", None, namespaces), record.findall("oai:metadata/xoai:metadata/xoai:element[@name='bundles']/xoai:element[@name='bundle']", namespaces)))
        thumbnails_xml = list(filter(lambda e: "THUMBNAIL" == e.findtext("xoai:field", None, namespaces), record.findall("oai:metadata/xoai:metadata/xoai:element[@name='bundles']/xoai:element[@name='bundle']", namespaces)))
        image = thumbnails_xml[0].findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='url']", None, namespaces) if thumbnails_xml else None
        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "type": lrts if lrts else ["Dataset"],
            "id": identifier,
            "name": dc_xml.findtext("xoai:element[@name='title']/xoai:element/xoai:field", None, namespaces),
            "image": image,
            "description": description.strip() if description else None,
            "inLanguage": [language_code] if language_code else None,
            "license": {"id": license_uri} if license_uri else None,
            "creator": list(filter(None, map(lambda s: build_creator(s.text), creator_xml))) if creator_xml else None,
            "contributor": list(filter(None, map(lambda s: {"type": "Person", "name": s.text.strip()} if s.text else None, contributor_xml))) if contributor_xml else None,
            "about": list(map(lambda s: {"id": s}, subjects)),
            "keywords": keywords,
            "sourceOrganization": [{"type": "Organization", "name": "Technische Universität Berlin", "id": "https://ror.org/03v4gjf40"}],
            # "dateCreated": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='issued']/xoai:element/xoai:field", None, namespaces),
            "datePublished": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='accessioned']/xoai:element/xoai:field", None, namespaces),
            "publisher": list(map(lambda s: {"type": "Organization", "name": s.text.strip()}, publisher_xml)),
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://depositonce.tu-berlin.de"
                    }
                }
            ],
            "encoding": list(map(lambda e: {
                "contentUrl": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='url']", None, namespaces),
                "encodingFormat": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='format']", None, namespaces),
                "contentSize": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='size']", None, namespaces)
            }, bundles_xml)),
            "conditionsOfAccess": {"id": "http://w3id.org/kim/conditionsOfAccess/no_login"}
        }
        return meta


if __name__ == "__main__":
    DepositOnceImport().process()
