import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject
from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class RADARMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("resourceType", {
            'Software': 'SoftwareApplication',
            'Dataset': 'Dataset',
            'Workflow': 'Guide',
            'Other': 'LearningResource',
            'Model': '3DModel',
            'Text': 'TextDigitalDocument',
            'Image': 'ImageObject',
            'Collection': 'Collection',
            'Audiovisual': 'MediaObject',
            'Sound': 'AudioObject'
        }, None)

        self.add_mapping("license", {
            'MIT License': 'https://opensource.org/licenses/MIT',
            'CC0 1.0 Universal Public Domain Dedication': 'https://creativecommons.org/publicdomain/zero/1.0/',
            'CC BY-ND 4.0 Attribution-NoDerivs': 'https://creativecommons.org/licenses/by-nd/4.0/',
            'CC BY-NC 4.0 Attribution-NonCommercial': 'https://creativecommons.org/licenses/by-nc/4.0/',
            'CC BY-NC-ND 4.0 Attribution-NonCommercial-NoDerivs': 'https://creativecommons.org/licenses/by-nc-nd/4.0/',
            'CC BY-NC-SA 4.0 Attribution-NonCommercial-ShareAlike': 'https://creativecommons.org/licenses/by-nc-sa/4.0/',
            'CC BY 4.0 Attribution': 'https://creativecommons.org/licenses/by/4.0/',
            'CC BY-SA 4.0 Attribution-ShareAlike': 'https://creativecommons.org/licenses/by-sa/4.0/',
            'GNU Lesser General Public License v3.0 only': 'https://www.gnu.org/licenses/lgpl-3.0',
            'GNU General Public License v3.0 only': 'https://www.gnu.org/licenses/gpl-3.0',
            'Apache License 2.0': 'https://www.apache.org/licenses/LICENSE-2.0',
            'Other': None,
            'All rights reserved': None
        }, None)

        self.add_mapping("subject", {
            'Agriculture': Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,
            'Architecture': Subject.N013_ARCHITECTURE,
            'Arts and Media': Subject.N75_FINE_ARTS,
            'Astrophysics and Astronomy': Subject.N014_ASTROPHYSICS_ASTRONOMY,
            'Biochemistry': Subject.N025_BIOCHEMISTRY,
            'Biology': Subject.N026_BIOLOGY,
            'Chemistry': Subject.N032_CHEMISTRY,
            'Computer Science': Subject.N079_COMPUTER_SCIENCE,
            'Economics': Subject.N175_ECONOMICS,
            'Engineering': Subject.N61_ENGINEERING_GENERAL,
            'Environmental Science and Ecology': Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            'Geography': Subject.N050_GEOGRAPHY,
            'Geological Science': Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            'History': Subject.N068_HISTORY,
            'Information Technology': Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            'Life Science': Subject.N186_AREA_OF_STUDY_NATURAL_SCIENCES_GENERAL_STUDIES,
            'Linguistics': Subject.N152_GENERAL_LINGUISTICS_INDOEUROPEAN_STUDIES,
            'Materials Science': Subject.N294_MATERIALS_SCIENCE,
            'Mathematics': Subject.N105_MATHEMATICS,
            'Medicine': Subject.N107_MEDICINE_GENERAL_MEDICINE,
            'Other': None,
            'Philosophy': Subject.N127_PHILOSOPHY,
            'Physics': Subject.N128_PHYSICS,
            'Psychology': Subject.N132_PSYCHOLOGY,
            'Social Sciences': Subject.N148_SOCIAL_SCIENCES,
            'Software Technology': Subject.N123_COMPUTATIONAL_ENGINEERING_TECHNICAL_COMPUTER_SCIENCE,
            'Sports': Subject.N2_SPORTS
        }, None)