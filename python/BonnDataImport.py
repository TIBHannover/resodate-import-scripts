from DataverseHelpers import DataverseImportBase
from search_index_import_commons.Helpers import get
from BonnDataMapping import BonnDataMapping
import re

class BonnDataImport(DataverseImportBase):

    def __init__(self):
        super().__init__("BonnData", "bonndata.uni-bonn.de", mapping=BonnDataMapping())

    def to_search_index_metadata(self, record):
        if ('versionState', 'DEACCESSIONED') not in record.items():
            meta = self.map_default(record)

            if "image" not in meta or not meta["image"]:
                meta["image"] = "https://bonndata.uni-bonn.de/imglibs/dataverse_logo.svg"
            meta["sourceOrganization"] = [{"type": "Organization", "name": "Universität Bonn", "id": "https://ror.org/041nas322"}]
            src_license = get(record["data"]["latestVersion"], "license")
            if src_license:
                src_license_uri = src_license["uri"].replace("https://mit-license.org/", "https://opensource.org/licenses/MIT")
                meta["license"] = {"id": src_license_uri}

            src_metadata_language = get(record["data"], "metadataLanguage")
            if (src_metadata_language is not None) and (src_metadata_language != "de"):
                position = meta["@context"].index({"@language": "de"})
                meta["@context"][position]["@language"] = src_metadata_language.replace("de-DE","de")

            language_path_to = list(filter(lambda s: s["typeName"] == "language", record['data']["latestVersion"]["metadataBlocks"]["citation"]["fields"]))
            if len(language_path_to)>0:
                meta["inLanguage"] = list(map(lambda l: self.mapping.get("language", l.strip()) if l else None, language_path_to[0]["value"]))

            if len(meta["about"]) == 0:
                alt_about_list = list(filter(lambda s: s["typeName"] == "subjectRefinement", record['data']["latestVersion"]["metadataBlocks"]["citation"]["fields"]))
                refinement_subject = alt_about_list[0]['value'][0]['subjectRefinementValue']['value']
                alternative_about = list(filter(None, map(lambda a: self.mapping.get("subject", a), [refinement_subject])))
                meta['about'] = list(map(lambda s: {"id": s}, alternative_about))
            pattern = re.compile("^(application|audio|example|font|image|message|model|multipart|text|video)/[a-zA-Z0-9\\-+_.]+$")
            meta["encoding"] = list(filter(lambda d: re.match(pattern, d["encodingFormat"]), meta["encoding"]))
            return meta

if __name__ == "__main__":
    BonnDataImport().process()
