from GitMetadataMapping import GitMetadataMapping
from search_index_import_commons.GitMetadataConverter import GitMetadataConverter
from search_index_import_commons.Helpers import get


def init_list(record, field_name):
    if field_name not in record:
        return []
    value = record[field_name]
    return value if isinstance(value, list) else [value]


def get_creator_name(creator):
    if "name" in creator:
        return creator["name"]
    names = []
    if "givenName" in creator:
        names.append(creator["givenName"])
    if "familyName" in creator:
        names.append(creator["familyName"])
    return " ".join(names) if len(names) > 0 else None


class Converter(GitMetadataConverter):
    def __init__(self):
        self.mapping = GitMetadataMapping()

    def transform(self, record, provider_name, provider_id):
        lrts = list(set(filter(None, map(lambda s: self.mapping.get("lrt", s if isinstance(s, str) else s["id"]), init_list(record, "learningResourceType")))))
        creator = list(map(lambda c: {"type": c["type"] if "type" in c else "Person", "name": get_creator_name(c), "id": get(c, "id"), "affiliation": get(c, "affiliation")}, init_list(record, "creator")))
        organizations = init_list(record, "sourceOrganization")
        organizations += list(filter(None, map(lambda c: get(c, "affiliation"), creator)))
        record_license = None
        if "license" in record:
            record_license = {"id": record["license"]} if isinstance(record["license"], str) else record["license"]
        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "type": lrts if lrts else ["Dataset"],
            "id": record["id"] if "id" in record else get(record["git_metadata"], "identifier"),
            "name": get(record, "name"),
            "conditionsOfAccess": {"id": "https://w3id.org/kim/conditionsOfAccess/no_login"},
            "image": record["image"] if "image" in record else get(record, "thumbnailUrl"),
            "description": get(record, "description"),
            "inLanguage": init_list(record, "inLanguage"),
            "license": record_license,
            "creator": creator,
            "dateCreated": get(record, "dateCreated"),
            "datePublished": get(record, "datePublished"),
            "about": init_list(record, "about"),
            "keywords": get(record, "keywords"),
            "sourceOrganization": organizations,
            "mainEntityOfPage": [
                {
                    "id": get(record["git_metadata"], "source_url"),
                    "provider": {
                        "type": "Service",
                        "name": provider_name,
                        "id": provider_id
                    }
                }
            ]
        }
        return meta
