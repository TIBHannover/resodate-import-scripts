from search_index_import_commons.GitHubImporterBase import GitHubImporterBase
from GitMetadataConverter import Converter


class GitHubImport(GitHubImporterBase):
    def __init__(self):
        super().__init__(Converter(), "research-software")
        self.allowed_statuses = self.config["git"]["allowed_statuses"] if "git" in self.config and "allowed_statuses" in self.config["git"] else ["Published"]

    def should_import(self, metadata) -> bool:
        return "creativeWorkStatus" in metadata and metadata["creativeWorkStatus"] == "Published"


if __name__ == "__main__":
    GitHubImport().process()
