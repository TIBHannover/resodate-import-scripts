from DataverseHelpers import DataverseImportBase
from search_index_import_commons.Helpers import get
from GROdataMapping import GROdataMapping


class GROdataImport(DataverseImportBase):

    def __init__(self):
        super().__init__("GRO.data", "data.goettingen-research-online.de", mapping=GROdataMapping())

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        if "image" not in meta or not meta["image"]:
            meta["image"] = "https://data.goettingen-research-online.de/assets/images/gro-data-large-white.svg"
        meta["sourceOrganization"] = [{"type": "Organization", "name": "Georg-August-Universität Göttingen", "id": "https://ror.org/01y9bpm73"}]
        src_license = get(record["data"]["latestVersion"], "license")
        if src_license:
            meta["license"] = {"id": src_license["uri"]}
        return meta


if __name__ == "__main__":
    GROdataImport().process()
