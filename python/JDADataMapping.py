from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject

class JDADataMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("subject", {
            "Other": None
        }, None)

        self.add_mapping("resourceType", {
            "dataset": "Dataset"
        }, None)

        self.add_mapping("language", {
            "English": "en",
            "German": "de"
        }, None)

        self.add_mapping("jel", {
            "A": Subject.N30_BUSINESS_AND_ECONOMICS,
            "B": Subject.N30_BUSINESS_AND_ECONOMICS,
            "C": Subject.N30_BUSINESS_AND_ECONOMICS,
            "D": Subject.N175_ECONOMICS,
            "E": Subject.N175_ECONOMICS,
            "F": Subject.N175_ECONOMICS,
            "G": Subject.N175_ECONOMICS,
            "H": Subject.N30_BUSINESS_AND_ECONOMICS,
            "I": Subject.N30_BUSINESS_AND_ECONOMICS,
            "J": Subject.N30_BUSINESS_AND_ECONOMICS,
            "K": Subject.N042_BUSINESS_LAW,
            "L": Subject.N30_BUSINESS_AND_ECONOMICS,
            "M": Subject.N021_BUSINESS_ADMINISTRATION,
            "N": Subject.N30_BUSINESS_AND_ECONOMICS,
            "O": Subject.N30_BUSINESS_AND_ECONOMICS,
            "P": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Q": Subject.N30_BUSINESS_AND_ECONOMICS,
            "R": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Y": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Z": Subject.N30_BUSINESS_AND_ECONOMICS
        }, None)

        self.add_mapping("owner_org", {
            "ffd970f3-110e-4247-91a4-21e4dc01b6d6": "International Journal for Re-Views in Empirical Economics",
            "18789d84-f6a4-4bd2-80d4-ee1242363f20": "JCRE",
            "2cb701e3-2735-4958-8cc7-b8418de587db": "Journal of Economics and Statistics",
            "38afea3e-dc9a-4147-8170-eddf5b44fae8": "VSWG - Journal of Social and Economic History",
            "44e42899-8e47-48a0-83e1-efb7ec960269": "German Economic Review",
            "808b8167-af26-45f3-b344-52b247938699": "Journal of Applied Econometrics",
            "8feb0989-5929-4be6-a779-ad1ec9bd217c": "Intereconomics"
        }, None)
