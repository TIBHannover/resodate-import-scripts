from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class LDMMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("type", {
            "dataset": "Dataset",
            "service": "Dataset",
            "vdataset": "Dataset"
        }, None)

        self.add_mapping("license", {
            "Apache License 2.0": "https://www.apache.org/licenses/LICENSE-2.0",
            "CC0-1.0": "https://creativecommons.org/publicdomain/zero/1.0/",
            "CC0 1.0 Universal Public Domain Dedication": "https://creativecommons.org/publicdomain/zero/1.0/",
            "cc-by": "https://creativecommons.org/licenses/by/4.0/",
            "CC-BY-3.0": "https://creativecommons.org/licenses/by/3.0/",
            "CC-BY-4.0": "https://creativecommons.org/licenses/by/4.0/",
            "CC BY 4.0 Attribution" : "https://creativecommons.org/licenses/by/4.0/",
            "CC-BY-NC-3.0": "https://creativecommons.org/licenses/by-nc/3.0/",
            "CC-BY-NC-4.0": "https://creativecommons.org/licenses/by-nc/4.0/",
            "CC BY-NC 4.0 Attribution-NonCommercial": "https://creativecommons.org/licenses/by-nc/4.0/",
            "CC-BY-NC-ND-3.0": "https://creativecommons.org/licenses/by-nc-nd/3.0/",
            "CC BY-NC-ND 4.0 Attribution-NonCommercial-NoDerivs": "https://creativecommons.org/licenses/by-nc-nd/4.0/",
            "CC-BY-NC-SA-4.0": "https://creativecommons.org/licenses/by-nc-sa/4.0/",
            "CC BY-NC-SA 4.0 Attribution-NonCommercial-ShareAlike": "https://creativecommons.org/licenses/by-nc-sa/4.0/",
            "CC BY-ND 4.0 Attribution-NoDerivs": "https://creativecommons.org/licenses/by-nd/4.0/",
            "cc-by-sa": "https://creativecommons.org/licenses/by-sa/4.0/",
            "CC-BY-SA-3.0": "https://creativecommons.org/licenses/by-sa/3.0/",
            "CC BY-SA 4.0 Attribution-ShareAlike": "https://creativecommons.org/licenses/by-sa/4.0/",
            "cc-nc": "https://creativecommons.org/licenses/by-nc/4.0/",
            "cc-zero": "https://creativecommons.org/publicdomain/zero/1.0/",
            "GNU General Public License v3.0 only": "https://www.gnu.org/licenses/gpl-3.0",
            "GNU Lesser General Public License v3.0 only": "https://www.gnu.org/licenses/lgpl-3.0",
            "MIT License": "https://opensource.org/license/mit",
            "ODbL-1.0": "https://opendatacommons.org/licenses/odbl/1-0/",
            "": None,
            "other-open": None,
            "All rights reserved": None,
            "notspecified": None,
            "Other": None
        }, None)

        self.add_mapping("orga_to_subject", {
            "Fachgebiet Wissensbasierte Systeme": Subject.N71_COMPUTER_SCIENCE,
            "Institut für Dynamik und Schwingungen": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Turbomaschinen und Fluid-Dynamik (TFD)": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "AG Frahm": Subject.N128_PHYSICS,
            "AG PALM": Subject.N110_METEOROLOGY,
            "Ernährungsphysiologie und Humanernährung": Subject.N60_NUTRITIONAL_AND_DOMESTIC_SCIENCE,
            "Fachgebiet Empirical Information Security": Subject.N71_COMPUTER_SCIENCE,
            "Geodätisches Institut Hannover": Subject.N171_SURVEYING_GEODESY,
            "IGPS: Abteilung Gehölz- und Vermehrungsphysiologie": Subject.N060_HORTICULTURE,
            "IGPS: Abteilung Phytomedizin": Subject.N060_HORTICULTURE,
            "Institut fuer Meteorologie und Klimatologie": Subject.N110_METEOROLOGY,
            "Institut fuer Mikroelektronische Systeme": Subject.N157_MICROELECTRONICS,
            "Institut für Baumechanik und Numerische Mechanik": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Baustoffe": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Didaktik der Mathematik und Physik": Subject.N33_EDUCATIONAL_SCIENCES,
            "Institut für Elektrische Energiesysteme": Subject.N316_ELECTRICAL_POWER_ENGINEERING,
            "Institut für Entwerfen und Konstruieren": Subject.N013_ARCHITECTURE,
            "Institut für Erdmessung": Subject.N171_SURVEYING_GEODESY,
            "Institut für Festkörperphysik": Subject.N39_PHYSICS_ASTRONOMY,
            "Institut für Gartenbauliche Produktionssysteme": Subject.N060_HORTICULTURE,
            "Institut für Geologie": Subject.N065_GEOLOGY_PALAEONTOLOGY,
            "Institut für Gravitationsphysik": Subject.N39_PHYSICS_ASTRONOMY,
            "Institut für Grundlagen der Elektrotechnik und Messtechnik": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Institut für Hydrologie und Wasserwirtschaft": Subject.N077_WATER_MANAGEMENT,
            "Institut für Kartographie und Geoinformatik": Subject.N280_CARTOGRAPHY,
            "Institut für Kommunikationstechnik": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,
            "Institut für Kontinuumsmechanik": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Massivbau": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Mess- und Regelungstechnik": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Mineralogie": Subject.N111_MINERALOGY,
            "Institut für Pflanzengenetik": Subject.N42_BIOLOGY,
            "Institut für Physische Geographie und Landschaftsökologie": Subject.N44_GEOGRAPHY,
            "Institut für Politikwissenschaft": Subject.N25_POLITICAL_SCIENCE,
            "Institut für Produktentwicklung und Gerätebau": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Produktionswirtschaft": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Institut für Quantenoptik": Subject.N39_PHYSICS_ASTRONOMY,
            "Institut für Radioökologie und Strahlenschutz": Subject.N4_MATHEMATICS_NATURAL_SCIENCES,
            "Institut für Regelungstechnik": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Institut für Sonderpädagogik": Subject.N190_SPECIAL_NEEDS_EDUCATION,
            "Institut für Statik und Dynamik": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Technische Chemie": Subject.N40_CHEMISTRY,
            "Institut für Theoretische Physik": Subject.N39_PHYSICS_ASTRONOMY,
            "Institut für Umformtechnik und Umformmaschinen": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Umweltplanung": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Institut für Werkstoffkunde": Subject.N72_MATERIALS_SCIENCE_AND_MATERIALS_ENGINEERING,
            "L3S": Subject.N71_COMPUTER_SCIENCE,
            "Ludwig-Franzius-Institut für Wasserbau, Ästuar- und Küsteningenieurwesen": Subject.N094_HYDRAULIC_ENGINEERING,
            "Institut für Fertigungstechnik und Werkzeugmaschinen": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Siedlungswasserwirtschaft und Abfalltechnik": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Windenergiesysteme": Subject.N68_CIVIL_ENGINEERING,
            "Englisches Seminar": Subject.N10_ENGLISH_STUDIES_AMERICAN_STUDIES,
            "GRK 2657": None,
            "LUIS": Subject.N71_COMPUTER_SCIENCE,
            "MCA": None,
            "MHH": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,
            "Research Training Group 1991": Subject.N128_PHYSICS,
            "SFB 1153": Subject.N8_ENGINEERING_SCIENCES,
            "SFB 1368": Subject.N202_MANUFACTURING_TECHNOLOGY_PRODUCTION_ENGINEERING,
            "TIB": None,
            "i.c.sens": None,
            "CoyPu Project": Subject.N71_COMPUTER_SCIENCE,
            "Institute for Solar Energy Research GmbH (ISFH)": Subject.N8_ENGINEERING_SCIENCES,
            "Institutes für Entwerfen und Konstruieren": Subject.N013_ARCHITECTURE,
            "PANGAEA (Agriculture)": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "RADAR": None,
            "Universidad Politécnica de Madrid": None
        }, None)

        self.add_mapping("subject_area", {
            "Agriculture": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "Architecture": Subject.N013_ARCHITECTURE,
            "Arts and Media": Subject.N9_ART_ART_THEORY,
            "Astrophysics and Astronomy": Subject.N014_ASTROPHYSICS_ASTRONOMY,
            "Atmosphere": Subject.N39_PHYSICS_ASTRONOMY,
            "Biochemistry": Subject.N025_BIOCHEMISTRY,
            "BiologicalClassification": Subject.N026_BIOLOGY,
            "Biology": Subject.N42_BIOLOGY,
            "Biosphere": Subject.N42_BIOLOGY,
            "Chemistry": Subject.N032_CHEMISTRY,
            "Computer Science": Subject.N079_COMPUTER_SCIENCE,
            "Cryosphere": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Ecology": Subject.N42_BIOLOGY,
            "Economics": Subject.N175_ECONOMICS,
            "Engineering": Subject.N8_ENGINEERING_SCIENCES,
            "Environmental Science and Ecology": Subject.N458_ENVIRONMENTAL_PROTECTION,
            "Geography": Subject.N44_GEOGRAPHY,
            "Geological Science": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "History": Subject.N068_HISTORY,
            "HumanDimensions": Subject.N7_AGRICULTURAL_FOREST_AND_NUTRITIONAL_SCIENCES_VETERINARY_MEDICINE,
            "Information Technology": Subject.N079_COMPUTER_SCIENCE,
            "LakesRivers": Subject.N077_WATER_MANAGEMENT,
            "LandSurface": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Life Science": Subject.N42_BIOLOGY,
            "Linguistics": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS,
            "Lithosphere": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Materials Science": Subject.N294_MATERIALS_SCIENCE,
            "Mathematics": Subject.N105_MATHEMATICS,
            "Medicine": Subject.N107_MEDICINE_GENERAL_MEDICINE,
            "Paleontology": Subject.N065_GEOLOGY_PALAEONTOLOGY,
            "Philosophy": Subject.N04_PHILOSOPHY,
            "Physics": Subject.N128_PHYSICS,
            "Psychology": Subject.N132_PSYCHOLOGY,
            "Social Sciences": Subject.N147_SOCIAL_STUDIES,
            "Software Technology": Subject.N71_COMPUTER_SCIENCE,
            "Sports": Subject.N029_SPORTS_SCIENCE,

        }, None)
