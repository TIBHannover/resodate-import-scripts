import re
import requests
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.Helpers import get


def parse_author_name(author):
    name = re.split(", ?", author)
    name.reverse()
    return " ".join(name)


class IdMapping:
    def get(self, group, key):
        return key

    def log_missing_keys(self):
        return


class DataverseImportBase(ResodateImporter):

    def __init__(self, name, domain, mapping=IdMapping(), items_per_request=25):
        super().__init__(mapping=mapping)
        self.name = name
        self.base_url = "https://" + domain
        self.record_count = 0
        self.items_per_request = items_per_request

    def get_name(self):
        return self.name

    def load_next(self):
        headers = {"Accept": "application/json", "Content-Type": "application/json", "User-Agent": self.user_agent}
        params = {"per_page": self.items_per_request, "start": self.record_count, "type": "dataset", "q": "*", "show_entity_ids": "true"}
        resp = requests.get(self.base_url + "/api/search", params=params, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + self.get_name() + ": {}".format(resp))
        self.record_count += self.items_per_request
        records = resp.json()["data"]["items"]
        if len(records) == 0:
            return None
        for record in records:
            resp = requests.get(self.base_url + "/api/datasets/" + str(record["entity_id"]), headers=headers)
            if resp.status_code == 200:
                record["data"] = resp.json()["data"]
        return list(filter(lambda r: "data" in r, records))

    def map_default(self, record):
        subjects = list(filter(None, map(lambda s: self.mapping.get("subject", s), get(record, "subjects"))))
        files = list(filter(lambda f: not f["restricted"], record["data"]["latestVersion"]["files"]))
        encodings = list(map(lambda f: {
            "contentUrl": self.base_url + "/api/access/datafile/" + str(f["dataFile"]["id"]),
            "encodingFormat": f["dataFile"]["contentType"],
            "contentSize": str(f["dataFile"]["filesize"])
        }, files))
        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "type": ["Dataset"],
            "id": record["url"],
            "name": record["name"],
            "creator": list(map(lambda c: {"name": parse_author_name(c), "type": "Person"}, record["authors"])),
            "description": get(record, "description"),
            "about": list(map(lambda s: {"id": s}, subjects)),
            "keywords": get(record, "keywords"),
            "publisher": [{"name": get(record, "publisher"), "type": "Organization"}] if get(record, "publisher") else None,
            "datePublished": record["data"]["publicationDate"],
            "conditionsOfAccess": {"id": "http://w3id.org/kim/conditionsOfAccess/no_login"},
            "mainEntityOfPage": [
                {
                    "id": record["url"],
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": self.base_url
                    }
                }
            ],
            "encoding": encodings
        }
        return meta

