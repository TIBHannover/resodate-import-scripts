import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject
from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class TerminologyServiceMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("subject", {
            "201-02 Biophysics": Subject.N128_PHYSICS,
            "307 Condensed Matter Physics": Subject.N128_PHYSICS,
            "308 Optics, Quantum Optics and Physics of Atoms, Molecules and Plasmas": Subject.N128_PHYSICS,
            "309 Particles, Nuclei and Fields": Subject.N128_PHYSICS,
            "310 Statistical Physics, Soft Matter, Biological Physics, Nonlinear Dynamics": Subject.N128_PHYSICS,
            "311 Astrophysics and Astronomy": Subject.N014_ASTROPHYSICS_ASTRONOMY,
            "32 Physics": Subject.N128_PHYSICS,
            "409-06 Information Systems, Process and Knowledge Management": Subject.N079_COMPUTER_SCIENCE,
            "Agriculture": Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,
            "Architecture": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "Chemical and environmental engineering": Subject.N033_CHEMICAL_ENGINEERING_CHEMICAL_PROCESS_ENGINEERING,
            "Chemistry": Subject.N40_CHEMISTRY,
            "Civil Engineering": Subject.N68_CIVIL_ENGINEERING,
            "Computer Science": Subject.N079_COMPUTER_SCIENCE,
            "Earth sciences": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Economics": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Educational science": Subject.N33_EDUCATIONAL_SCIENCES,
            "Electrical engineering": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Engineering": Subject.N61_ENGINEERING_GENERAL,
            "General": None,
            "History": Subject.N05_HISTORY,
            "Horticulture": Subject.N060_HORTICULTURE,
            "Law": Subject.N28_LAW,
            "Life Sciences, biology": Subject.N42_BIOLOGY,
            "Life Sciences, biologyextension": Subject.N42_BIOLOGY,
            "Life sciences, biology": Subject.N42_BIOLOGY,
            "Linguistics": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS,
            "Literature": Subject.N07_GENERAL_AND_COMPARATIVE_LITERARY_STUDIES_AND_LINGUISTICS,
            "Material science": Subject.N294_MATERIALS_SCIENCE,
            "Mathematics": Subject.N37_MATHEMATICS,
            "Mechanical engineering, energy technology": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Medicine": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,
            "Physics": Subject.N39_PHYSICS_ASTRONOMY,
            "Social sciences": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,
            "Traffic engineering": Subject.N65_TRAFFIC_ENGINEERING_NAUTICAL_SCIENCE,
        }, None)
