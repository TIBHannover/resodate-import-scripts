import re
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader
from DataCiteMapping import DataCiteMapping


def namespace(element):
    m = re.match(r'\{(.*)\}', element.tag)
    return m.group(1) if m else ''


def build_creator(creator, namespaces, affiliation_regex="*"):
    name = creator.findtext("datacite:givenName", default="", namespaces=namespaces) + " " + creator.findtext("datacite:familyName", default="", namespaces=namespaces)
    if not name.strip():
        c_name = re.split(", ?", creator.findtext("datacite:creatorName", default="", namespaces=namespaces))
        c_name.reverse()
        name = " ".join(c_name)
    affiliation = creator.findtext("datacite:affiliation", default=None, namespaces=namespaces)
    orcid = creator.findtext("datacite:nameIdentifier[@ nameIdentifierScheme='ORCID']", None, namespaces)
    if orcid:
        orcid = orcid.replace(' ', '')
    return {
        "type": "Person",
        "name": name.strip(),
        "id": ("https://orcid.org/" + orcid) if orcid else None,
        "affiliation": {"type": "Organization", "name": affiliation} if affiliation and re.match(affiliation_regex, affiliation) else None
    }


def build_contributor(contributor, namespaces):
    c_name = re.split(", ?", contributor.findtext("datacite:contributorName", default="", namespaces=namespaces))
    c_name.reverse()
    name = " ".join(c_name)
    return {
        "type": "Person",
        "name": name.strip()
    }


def to_date(datetime_string):
    if datetime_string is not None:
        if re.match("^\\d{4}-\\d{2}-\\d{2}[T ]\\d{2}:\\d{2}:\\d{2}", datetime_string):
            return datetime_string[:19].replace(" ", "T") + "Z"
        elif re.match("^\\d{4}-\\d{2}-\\d{2}", datetime_string):
            return datetime_string[:10]
        elif re.match("^\\d{4}", datetime_string):
            return datetime_string[:4] + "-01-01"
    return None


class LeoPARDImport(ResodateImporter):

    subjectSchemes = ["ddc"]
    affiliation_regex = "(?i).*university.*"

    def __init__(self):
        super().__init__(mapping=DataCiteMapping())
        self.oai = OaiPmhReader("https://leopard.tu-braunschweig.de/servlets/OAIDataProvider", "oai_datacite", "GENRE:research_data", self.user_agent)

    def get_name(self):
        return "LeoPARD"

    def load_next(self):
        return self.oai.load_next()

    def to_search_index_metadata(self, record):
        namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/", "oai_datacite": "http://datacite.org/schema/kernel-4"}
        record_status = record.find("oai:header", namespaces)
        if "status" in record_status.attrib and record_status.get("status") == "deleted":
            return None
        resource_xml = record.find("oai:metadata/oai_datacite:resource", namespaces)
        namespaces = {"datacite": namespace(resource_xml)}

        doi = resource_xml.findtext("datacite:identifier[@identifierType='DOI']", None, namespaces)

        subjects_xml = resource_xml.findall("datacite:subjects/datacite:subject", namespaces)
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s.text), filter(lambda s: s.get("subjectScheme") in self.subjectSchemes, subjects_xml))))
        keywords = list(set(filter(None, map(lambda s: s.text, filter(lambda s: s.get("subjectScheme") not in self.subjectSchemes, subjects_xml)))))
        license_uri = self.mapping.get("license", resource_xml.findtext("datacite:rights", None, namespaces))
        if not license_uri:
            rights_node = resource_xml.find("datacite:rightsList/datacite:rights[@rightsURI]", namespaces)
            if rights_node is not None:
                license_uri = self.mapping.get("license", rights_node.get("rightsURI"))
        creator_xml = resource_xml.findall("datacite:creators/datacite:creator", namespaces)
        contributor_xml = resource_xml.findall("datacite:contributors/datacite:contributor", namespaces)
        lrts = list(set(filter(None, map(lambda t: self.mapping.get("lrt", t.text), resource_xml.findall("datacite:resourceType", namespaces)))))
        publication_date = resource_xml.findtext("datacite:dates/datacite:date[@dateType='Issued']", None, namespaces)
        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "type": lrts if lrts else ["Dataset"],
            "id": "https://doi.org/" + doi,
            "name": resource_xml.findtext("datacite:titles/datacite:title", None, namespaces),
            "description": resource_xml.findtext("datacite:descriptions/datacite:description", None, namespaces),
            "inLanguage": list(filter(None, map(lambda l: self.mapping.get("language", l.text.strip()) if l.text else None, resource_xml.findall("datacite:language", namespaces)))),
            "license": {"id": license_uri} if license_uri else None,
            "creator": list(filter(None, map(lambda c: build_creator(c, namespaces, self.affiliation_regex), creator_xml))) if creator_xml is not None else None,
            "contributor": list(filter(None, map(lambda c: build_contributor(c, namespaces), contributor_xml))) if contributor_xml is not None else None,
            "about": list(map(lambda s: {"id": s}, subjects)),
            "keywords": keywords,
            "sourceOrganization": [{"type": "Organization", "name": "Technische Universität Braunschweig", "id": "https://ror.org/010nsgg66"}],
            "datePublished": to_date(publication_date),
            "publisher": list(filter(None, map(lambda s: {"type": "Organization", "name": s.text}, resource_xml.findall("datacite:publisher", namespaces)))),
            "mainEntityOfPage": [
                {
                    "id": resource_xml.findtext("datacite:alternateIdentifiers/datacite:alternateIdentifier[@alternateIdentifierType='URL']", None, namespaces),
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://leopard.tu-braunschweig.de"
                    },
                    "dateCreated": to_date(resource_xml.findtext("datacite:dates/datacite:date[@dateType='Created']", None, namespaces)),
                    "dateModified": to_date(resource_xml.findtext("datacite:dates/datacite:date[@dateType='Updated']", None, namespaces))
                }
            ]
        }
        return meta


if __name__ == "__main__":
    LeoPARDImport().process()
