import requests
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.Helpers import get, parse_entity_name

class IdMapping:
    def get(self, group, key):
        return key

    def log_missing_keys(self):
        return

class CKANImportBase(ResodateImporter):

    def __init__(self, name, domain, mapping=IdMapping(), items_per_request=100):
        super().__init__(mapping=mapping)
        self.name = name
        self.base_url = "https://" + domain
        self.record_count = 0
        self.items_per_request = items_per_request

    def get_name(self) -> str:
        return self.name

    def load_next(self):
        headers = {"Accept": "application/json", "Content-Type": "application/json", "User-Agent": self.user_agent}
        params = {"rows": self.items_per_request, "start": self.record_count}
        resp = requests.get(self.base_url + "/api/action/package_search", params=params, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + self.get_name() + ": {}".format(resp))
        self.record_count += self.items_per_request
        records = resp.json()["result"]["results"]
        if len(records) == 0:
            return None
        return records

    def map_default(self, record):
        identifier = self.base_url + "/dataset/" + record["name"]
        description = get(record, "notes")
        keywords = list(map(lambda r: r["display_name"], record["tags"]))
        encodings = list(filter(None, map(lambda r: {
            "contentUrl": r["url"],
            "encodingFormat": r["mimetype"],
            "contentSize": str(r["size"])
        }, filter(lambda r: r["url_type"] == "upload", record["resources"]))))

        meta = {
            "@context": [
                "https://schema.org"
            ],
            "id": identifier,
            "name": record["title"],
            "description": description,
            "keywords": keywords,
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": self.base_url
                    },
                    "dateCreated": get(record, "metadata_created")[:19] + "Z",
                    "dateModified": get(record, "metadata_modified")[:19] + "Z"
                }
            ],
            "encoding": encodings
        }
        return meta
