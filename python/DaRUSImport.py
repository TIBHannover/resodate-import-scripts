from DataverseHelpers import DataverseImportBase
from search_index_import_commons.Helpers import get
from DaRUSMapping import DaRUSMapping


class DaRUSImport(DataverseImportBase):

    def __init__(self):
        super().__init__("DaRUS", "darus.uni-stuttgart.de", mapping=DaRUSMapping())

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        for file in meta['encoding']:
            if self.mapping.get('mimetype', file['encodingFormat']):
                file['encodingFormat'] = self.mapping.get('mimetype', file['encodingFormat'])
        if "image" not in meta or not meta["image"]:
            meta["image"] = "https://darus.uni-stuttgart.de/logos/navbar/darus-long-white_h60px.png"
        meta["sourceOrganization"] = [{"type": "Organization", "name": "Universität Stuttgart", "id": "https://ror.org/04vnq7t77"}]
        src_license = get(record["data"]["latestVersion"], "license")
        if src_license:
            meta["license"] = {"id": src_license["uri"]}
        return meta


if __name__ == "__main__":
    DaRUSImport().process()
