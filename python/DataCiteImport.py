import base64
import re
import urllib.parse
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader
from DataCiteMapping import DataCiteMapping
import html2text

def namespace(element):
    m = re.match(r'\{(.*)\}', element.tag)
    return m.group(1) if m else ''


def build_creator(creator, namespaces):
    name = creator.findtext("datacite:givenName", default="", namespaces=namespaces) + " " + creator.findtext("datacite:familyName", default="", namespaces=namespaces)
    if not name.strip():
        c_name = re.split(", ?", creator.findtext("datacite:creatorName", default="", namespaces=namespaces))
        c_name.reverse()
        name = " ".join(c_name)
    affiliation = creator.findtext("datacite:affiliation", default=None, namespaces=namespaces)
    orcid = creator.findtext("datacite:nameIdentifier[@ nameIdentifierScheme='ORCID']", None, namespaces)
    if orcid:
        orcid = orcid.replace(' ', '')
    return {
        "type": "Person",
        "name": name.strip(),
        "id": ("https://orcid.org/" + orcid) if orcid else None,
        "affiliation": {"type": "Organization", "name": affiliation} if affiliation else None
    }


def build_contributor(contributor, namespaces):
    c_name = re.split(", ?", contributor.findtext("datacite:contributorName", default="", namespaces=namespaces))
    c_name.reverse()
    name = " ".join(c_name)
    return {
        "type": "Person",
        "name": name.strip()
    }


def to_date(datetime_string):
    if datetime_string is not None:
        if re.match("^\\d{4}-\\d{2}-\\d{2}[T ]\\d{2}:\\d{2}:\\d{2}", datetime_string):
            return datetime_string[:19].replace(" ", "T") + "Z"
        elif re.match("^\\d{4}-\\d{2}-\\d{2}", datetime_string):
            return datetime_string[:10]
    return None


class DataCiteImport(ResodateImporter):

    subjectSchemes = ["dewey", "ddc", "DDC", "Fields of Science and Technology (FOS)"]
    subject_filter = ["FOS: Civil engineering", "FOS: Mechanical engineering", "FOS: Chemical engineering", "51*", "53*", "54*", "62*"]
    publisher_complete_filter = ["Open Research Knowledge Graph"]  # add resources of this publishers
    type_filter = ["Dataset", "Software"]

    def __init__(self):
        super().__init__(mapping=DataCiteMapping())
        query = 'types.resourceTypeGeneral:(' + " OR ".join(self.type_filter) + ')'
        if self.subject_filter:
            query += ' AND subjects.subject:(' + " OR ".join(list(map(lambda s: s if '*' in s else ('"' + s + '"'), self.subject_filter))) + ')'
        query = '(' + query + ') OR publisher:(' + " OR ".join(self.publisher_complete_filter) + ')'
        oai_set = "~" + base64.urlsafe_b64encode(str.encode(urllib.parse.quote(query))).decode("utf-8")
        self.oai = OaiPmhReader("https://oai.datacite.org/oai", "oai_datacite", oai_set, self.user_agent)
        self.html2text = html2text.HTML2Text(bodywidth=0)
        self.html2text.ignore_links = True
        self.html2text.ignore_emphasis = True

    def get_name(self):
        return "DataCite"

    def load_next(self):
        return self.oai.load_next()

    def to_search_index_metadata(self, record):
        namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/", "oai_datacite": "http://schema.datacite.org/oai/oai-1.1/"}
        record_status = record.find("oai:header", namespaces)
        if "status" in record_status.attrib and record_status.get("status") == "deleted":
            return None
        payload_xml = record.find("oai:metadata/oai_datacite:oai_datacite/oai_datacite:payload", namespaces)
        resource_xml = payload_xml[0] if len(payload_xml) else None
        namespaces = {"datacite": namespace(resource_xml)}

        doi = resource_xml.findtext("datacite:identifier[@identifierType='DOI']", None, namespaces)

        subjects_xml = resource_xml.findall("datacite:subjects/datacite:subject", namespaces)
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s.text), filter(lambda s: s.get("subjectScheme") in self.subjectSchemes, subjects_xml))))
        keywords = list(set(filter(None, map(lambda s: s.text, filter(lambda s: s.get("subjectScheme") not in self.subjectSchemes, subjects_xml)))))
        license_uri = self.mapping.get("license", resource_xml.findtext("datacite:rights", None, namespaces))
        if not license_uri:
            rights_node = resource_xml.find("datacite:rightsList/datacite:rights[@rightsURI]", namespaces)
            if rights_node is not None:
                license_uri = self.mapping.get("license", rights_node.get("rightsURI"))
        creator_xml = resource_xml.findall("datacite:creators/datacite:creator", namespaces)
        contributor_xml = resource_xml.findall("datacite:contributors/datacite:contributor", namespaces)
        lrts = list(set(filter(None, map(lambda t: self.mapping.get("lrt", t.attrib["resourceTypeGeneral"]), resource_xml.findall("datacite:resourceType[@resourceTypeGeneral]", namespaces)))))
        description_text = resource_xml.findtext("datacite:descriptions/datacite:description", None, namespaces)
        description_cleanedup = self.html2text.handle(description_text) if description_text else None
        publication_year = resource_xml.findtext("datacite:publicationYear", None, namespaces)
        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "type": lrts if lrts else ["Dataset"],
            "id": "https://doi.org/" + doi,
            "name": resource_xml.findtext("datacite:titles/datacite:title", None, namespaces),
            "description": description_cleanedup.replace("\n","") if description_cleanedup is not None else None,
            "inLanguage": list(filter(None, map(lambda l: self.mapping.get("language", l.text.strip()) if l.text else None, resource_xml.findall("datacite:language", namespaces)))),
            "license": {"id": license_uri} if license_uri else None,
            "creator": list(filter(None, map(lambda c: build_creator(c, namespaces), creator_xml))) if creator_xml is not None else None,
            "contributor": list(filter(None, map(lambda c: build_contributor(c, namespaces), contributor_xml))) if contributor_xml is not None else None,
            "about": list(map(lambda s: {"id": s}, subjects)),
            "keywords": keywords,
            "datePublished": (publication_year + "-01-01") if publication_year else None,
            "publisher": list(filter(None, map(lambda s: {"type": "Organization", "name": s.text}, resource_xml.findall("datacite:publisher", namespaces)))),
            "mainEntityOfPage": [
                {
                    "id": "https://commons.datacite.org/doi.org/" + doi,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://commons.datacite.org"
                    },
                    "dateCreated": to_date(resource_xml.findtext("datacite:dates/datacite:date[@dateType='Created']", None, namespaces)),
                    "dateModified": to_date(resource_xml.findtext("datacite:dates/datacite:date[@dateType='Updated']", None, namespaces))
                }
            ]
        }
        return meta


if __name__ == "__main__":
    DataCiteImport().process()
