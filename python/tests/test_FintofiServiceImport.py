import unittest
from unittest import mock
from FintofiServiceImport import FintofiServiceImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse
import json
from urllib.parse import urlparse
import re
from typing import Any


def mocked_requests_get(*args, **kwargs):
    url = urlparse(args[0]).path if len(args) > 0 else ""
    with open('tests/resources/FintofiService_001.json') as f:
        content = json.loads(f.read())

    # regular expressions for determining request type
    vocabularies_regex = re.compile(r'^/rest/v1/vocabularies$')
    yso_regex = re.compile(r'^/rest/v1/yso(/data)?$')
    vocab_id_regex = re.compile(r'^/rest/v1/(?!yso$)(?!vocabularies$)[^/]+$')
    vocab_id_data_regex = re.compile(r'^/rest/v1/(?!yso)[^/]+/data$')

    # mock request for vocabulary IDs
    if bool(vocabularies_regex.match(url)):
        return MockResponse(content=json.dumps(content["all_vocabulary_ids"]), status_code=200)
    # mock request for concept scheme URIs
    if bool(vocab_id_regex.match(url)):
        return MockResponse(content=json.dumps(content["concept_schemes_uris"]), status_code=200)
    # mock request for vocabulary schema metadata
    if bool(vocab_id_data_regex.match(url)):
        return MockResponse(content=content["vocab_schema_rdf"]["rdf"], status_code=200)
    # mock request for subject groups
    if bool(yso_regex.match(url)):
        return MockResponse(content=content["subject_rdf"]["rdf"], status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


def deep_sort(obj: Any) -> Any:
    """
    Recursively sort lists of dictionaries within the given object.
    """
    if isinstance(obj, dict):
        return {k: deep_sort(v) for k, v in obj.items()}
    elif isinstance(obj, list):
        # Check if list contains dictionaries
        if all(isinstance(i, dict) for i in obj):
            return sorted((deep_sort(i) for i in obj), key=lambda x: sorted(x.items()))
        else:
            return sorted(deep_sort(i) for i in obj)
    else:
        return obj


class FintofiServiceImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        FintofiServiceImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        # sort 'about' value to match expected order
        result_metadata[0]['about'] = deep_sort(result_metadata[0]['about'])
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/FintofiService_expected_001.json')


if __name__ == '__main__':
    unittest.main()
