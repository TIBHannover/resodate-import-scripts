import unittest
from unittest import mock
from LeoPARDImport import LeoPARDImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://leopard.tu-braunschweig.de/servlets/OAIDataProvider':
        if "resumptionToken" in kwargs["params"]:
            return MockResponse(content='<?xml version="1.0" encoding="UTF-8"?><OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd" />', status_code=200)
        with open('tests/resources/LeoPARD_001.xml') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class LeoPARDImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        LeoPARDImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/LeoPARD_expected_001.json')


if __name__ == '__main__':
    unittest.main()
