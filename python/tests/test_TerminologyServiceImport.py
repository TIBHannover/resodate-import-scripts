import unittest
from unittest import mock
from TerminologyServiceImport import TerminologyServiceImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://service.tib.eu/ts4tib/api/ontologies':
        if kwargs["params"]["page"] == 0:
            with open('tests/resources/TerminologyService_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        else:
            return MockResponse(content='{}', status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class TerminologyServiceImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        TerminologyServiceImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assertEqual(len(result_metadata), 3)
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/TerminologyService_expected_001.json')
        self.assert_result_contains_expected_metadata(result_metadata[1], 'tests/resources/TerminologyService_expected_002.json')
        self.assert_result_contains_expected_metadata(result_metadata[2], 'tests/resources/TerminologyService_expected_003.json')


if __name__ == '__main__':
    unittest.main()
