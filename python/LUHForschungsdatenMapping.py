from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class LUHForschungsdatenMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("lrt", {
            "dataset": "Dataset"
        }, None)

        self.add_mapping("orga_to_subject", {
            "Fachgebiet Wissensbasierte Systeme": Subject.N71_COMPUTER_SCIENCE,
            "Institut für Dynamik und Schwingungen": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Turbomaschinen und Fluid-Dynamik (TFD)": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "AG Frahm": Subject.N128_PHYSICS,
            "AG PALM": Subject.N110_METEOROLOGY,
            "Ernährungsphysiologie und Humanernährung": Subject.N60_NUTRITIONAL_AND_DOMESTIC_SCIENCE,
            "Fachgebiet Empirical Information Security": Subject.N71_COMPUTER_SCIENCE,
            "Geodätisches Institut Hannover": Subject.N171_SURVEYING_GEODESY,
            "IGPS: Abteilung Gehölz- und Vermehrungsphysiologie": Subject.N060_HORTICULTURE,
            "IGPS: Abteilung Phytomedizin": Subject.N060_HORTICULTURE,
            "Institut fuer Meteorologie und Klimatologie": Subject.N110_METEOROLOGY,
            "Institut fuer Mikroelektronische Systeme": Subject.N157_MICROELECTRONICS,
            "Institut für Baumechanik und Numerische Mechanik": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Baustoffe": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Didaktik der Mathematik und Physik": Subject.N33_EDUCATIONAL_SCIENCES,
            "Institut für Elektrische Energiesysteme": Subject.N316_ELECTRICAL_POWER_ENGINEERING,
            "Institut für Entwerfen und Konstruieren": Subject.N013_ARCHITECTURE,
            "Institut für Erdmessung": Subject.N171_SURVEYING_GEODESY,
            "Institut für Festkörperphysik": Subject.N39_PHYSICS_ASTRONOMY,
            "Institut für Gartenbauliche Produktionssysteme": Subject.N060_HORTICULTURE,
            "Institut für Geologie": Subject.N065_GEOLOGY_PALAEONTOLOGY,
            "Institut für Gravitationsphysik": Subject.N39_PHYSICS_ASTRONOMY,
            "Institut für Grundlagen der Elektrotechnik und Messtechnik": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Institut für Hydrologie und Wasserwirtschaft": Subject.N077_WATER_MANAGEMENT,
            "Institut für Kartographie und Geoinformatik": Subject.N280_CARTOGRAPHY,
            "Institut für Kommunikationstechnik": Subject.N222_COMMUNICATION_TECHNOLOGY_INFORMATION_ENGINEERING,
            "Institut für Kontinuumsmechanik": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Massivbau": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Mess- und Regelungstechnik": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Mineralogie": Subject.N111_MINERALOGY,
            "Institut für Pflanzengenetik": Subject.N42_BIOLOGY,
            "Institut für Physische Geographie und Landschaftsökologie": Subject.N44_GEOGRAPHY,
            "Institut für Politikwissenschaft": Subject.N25_POLITICAL_SCIENCE,
            "Institut für Produktentwicklung und Gerätebau": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Produktionswirtschaft": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Institut für Quantenoptik": Subject.N39_PHYSICS_ASTRONOMY,
            "Institut für Radioökologie und Strahlenschutz": Subject.N4_MATHEMATICS_NATURAL_SCIENCES,
            "Institut für Regelungstechnik": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Institut für Sonderpädagogik": Subject.N190_SPECIAL_NEEDS_EDUCATION,
            "Institut für Statik und Dynamik": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Technische Chemie": Subject.N40_CHEMISTRY,
            "Institut für Theoretische Physik": Subject.N39_PHYSICS_ASTRONOMY,
            "Institut für Umformtechnik und Umformmaschinen": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Umweltplanung": Subject.N57_LAND_MANAGEMENT_ENGINEERING_ENVIRONMENTAL_DESIGN,
            "Institut für Werkstoffkunde": Subject.N72_MATERIALS_SCIENCE_AND_MATERIALS_ENGINEERING,
            "L3S": Subject.N71_COMPUTER_SCIENCE,
            "Ludwig-Franzius-Institut für Wasserbau, Ästuar- und Küsteningenieurwesen": Subject.N094_HYDRAULIC_ENGINEERING,
            "Institut für Fertigungstechnik und Werkzeugmaschinen": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Institut für Siedlungswasserwirtschaft und Abfalltechnik": Subject.N68_CIVIL_ENGINEERING,
            "Institut für Windenergiesysteme": Subject.N68_CIVIL_ENGINEERING,
            "Englisches Seminar": Subject.N10_ENGLISH_STUDIES_AMERICAN_STUDIES,
            "GRK 2657": None,
            "LUIS": Subject.N71_COMPUTER_SCIENCE,
            "MCA": None,
            "MHH": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,
            "Research Training Group 1991": Subject.N128_PHYSICS,
            "SFB 1153": Subject.N8_ENGINEERING_SCIENCES,
            "SFB 1368": Subject.N202_MANUFACTURING_TECHNOLOGY_PRODUCTION_ENGINEERING,
            "TIB": None,
            "i.c.sens": None
        }, None)
