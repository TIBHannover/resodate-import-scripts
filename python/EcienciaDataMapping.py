import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject
from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class EcienciaDataMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("subject", {
            "Administración y empresas": Subject.N021_BUSINESS_ADMINISTRATION,
            "Agricultural Sciences": Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,
            "Artes y humanidades": Subject.N01_HUMANITIES_GENERAL,
            "Arts and Humanities": Subject.N01_HUMANITIES_GENERAL,
            "Astronomía y astrofísica": Subject.N014_ASTROPHYSICS_ASTRONOMY,
            "Astronomy and Astrophysics": Subject.N014_ASTROPHYSICS_ASTRONOMY,
            "Business and Management": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Chemistry": Subject.N40_CHEMISTRY,
            "Ciencias agrarias": Subject.N003_AGRICULTURAL_SCIENCE_AGRICULTURE,
            "Ciencias de la información y computación": Subject.N71_COMPUTER_SCIENCE,
            "Ciencias de la tierra y el medioambiente": Subject.N186_AREA_OF_STUDY_NATURAL_SCIENCES_GENERAL_STUDIES,
            "Ciencias matemáticas": Subject.N37_MATHEMATICS,
            "Ciencias médicas, de la salud y de la vida": Subject.N5_HUMAN_MEDICINE__HEALTH_SCIENCES,
            "Ciencias sociales": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,
            "Computer and Information Science": Subject.N71_COMPUTER_SCIENCE,
            "Derecho": Subject.N28_LAW,
            "Earth and Environmental Sciences": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Engineering": Subject.N61_ENGINEERING_GENERAL,
            "Física": Subject.N39_PHYSICS_ASTRONOMY,
            "Ingeniería": Subject.N61_ENGINEERING_GENERAL,
            "Law": Subject.N28_LAW,
            "Mathematical Sciences": Subject.N37_MATHEMATICS,
            "Medicine, Health and Life Sciences": Subject.N49_HUMAN_MEDICINE_EXCL_DENTISTRY,
            "Other": None,
            "Otra": None,
            "Physics": Subject.N39_PHYSICS_ASTRONOMY,
            "Química": Subject.N40_CHEMISTRY,
            "Social Sciences": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY
        }, None)

        self.add_mapping("language", {
            "Catalan,Valencian": "ca",
            "Chinese": "zh",
            "English": "en",
            "French": "fr",
            "Galician": "gl",
            "German": "de",
            "Italian": "it",
            "Not applicable": None,
            "Portuguese": "pt",
            "Spanish": "es"
        }, None)

        self.add_mapping("institutions", {
            "UAM": "Universidad Autónoma de Madrid",
            "UAH": "Universidad de Alcalá",
            "UCM": "Universidad Carlos III de Madrid",
            "UNED": "Universidad Nacional de Educación a Distancia (UNED)",
            "UPM": "Universidad Politécnica de Madrid",
            "URJC": "Universidad Rey Juan Carlos"
        }, None)
