import abc
import logging
from langdetect import detect, lang_detect_exception
from search_index_import_commons.mapping import normalized_licences
from search_index_import_commons.SearchIndexImporter import SearchIndexImporter


def normalize_licenses(records):
    for record in records:
        if "license" in record:
            normalize_license(record["license"])

def normalize_license(license_meta):
    if not license_meta or "id" not in license_meta:
        return
    license_url = license_meta["id"]
    mapped_license_url = normalized_licences.mapping.get(license_url)
    if mapped_license_url:
        license_meta["id"] = mapped_license_url

def detect_language_if_missing(records):
    for record in records:
        context_language = "en"
        if not record.get("inLanguage"):
            description = record.get("description", "")
            if description and len(description.split(" ")) > 10:
                try:
                    language = detect(description)
                    if language:
                        context_language = language.replace("zh-cn", "zh").replace("zh-tw", "zh")
                        record["inLanguage"] = [context_language]
                except lang_detect_exception.LangDetectException:
                    logging.debug("Could not detect language for record %s", record["id"])
        add_language_to_context_if_missing(record, context_language)

def add_language_to_context_if_missing(record, context_language):
    meta_context = record.get("@context")
    if meta_context and isinstance(meta_context, list):
        context_languages = [ctx for ctx in meta_context if isinstance(ctx, dict) and "@language" in ctx]
        if not context_languages:
            meta_context.append({"@language": context_language})

class ResodateImporter(SearchIndexImporter):

    def contains_record(self, record_id):
        # not implement per default for resodate
        return False

    def load_single_record(self, record_id):
        # not implement per default for resodate
        return None

    def transform(self, data: list) -> list:
        transformed_data = list(filter(None, map(lambda record: self.to_search_index_metadata(record), data)))
        normalize_licenses(transformed_data)
        detect_language_if_missing(transformed_data)
        return transformed_data

    @abc.abstractmethod
    def to_search_index_metadata(self, record):
        """instance specific transformation of a record"""
