from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class TUWienResearchDataMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("resourceType", {
            'software': 'SoftwareApplication',
            'dataset': 'Dataset',
            'workflow': 'Guide',
            'presentation': 'PresentationDigitalDocument',
            'other': 'LearningResource',
            'model': '3DModel',
            'video': 'VideoObject',
            'text': 'TextDigitalDocument',
            'report': 'Report',
            'poster': 'Poster',
            'image': 'ImageObject',
            'computationalnotebook': 'SoftwareSourceCode'
        }, None)

        self.add_mapping("identifier", {
            'orcid': 'https://orcid.org/',
            'isni': 'https://isni.org/isni/'
        }, None)

        self.add_mapping("creatorType", {
            'organizational': 'Organization',
            'personal': 'Person'
        }, None)
