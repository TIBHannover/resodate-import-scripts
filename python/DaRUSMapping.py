import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject
from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class DaRUSMapping(SearchIndexImportMapping):

    def __init__(self):
        self.add_mapping("subject", {
            "Agricultural Sciences": Subject.N58_AGRICULTURAL_SCIENCE_FOOD_AND_BEVERAGE_TECHNOLOGY,
            "Arts and Humanities": Subject.N01_HUMANITIES_GENERAL,
            "Astronomy and Astrophysics": Subject.N014_ASTROPHYSICS_ASTRONOMY,
            "Computer and Information Science": Subject.N71_COMPUTER_SCIENCE,
            "Chemistry": Subject.N40_CHEMISTRY,
            "Earth and Environmental Sciences": Subject.N43_GEOSCIENCES_EXCL_GEOGRAPHY,
            "Engineering": Subject.N61_ENGINEERING_GENERAL,
            "Mathematical Sciences": Subject.N37_MATHEMATICS,
            "Medicine, Health and Life Sciences": Subject.N49_HUMAN_MEDICINE_EXCL_DENTISTRY,
            "Physics": Subject.N39_PHYSICS_ASTRONOMY,
            "Social Sciences": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY
        }, None)

        self.add_mapping('mimetype', {
            'text/plain; charset=US-ASCII': 'text/plain',
            'text/plain; charset=UTF-8': 'text/plain'
        }, None)
