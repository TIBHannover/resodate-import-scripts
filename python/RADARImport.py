from RADARMapping import RADARMapping
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.mapping.iso639_2_to_iso639_1 import mapping as lang_mapping
from search_index_import_commons.OaiPmhReader import OaiPmhReader
import xml.etree.ElementTree as ET
from typing import Optional
import re


class RADARImport(ResodateImporter):

    namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/",
                  "radar-dataset": "http://radar-service.eu/schemas/descriptive/radar/v09/radar-dataset",
                  "radar-elements": "http://radar-service.eu/schemas/descriptive/radar/v09/radar-elements"}

    organizations_map = {}

    def __init__(self):
        super().__init__(mapping=RADARMapping())
        self.loaded = False
        self.oai = OaiPmhReader("https://www.radar-service.eu/oai/OAIHandler", "radar", None, self.user_agent)
        self.organizations_map = {}

    def get_name(self):
        return "RADAR"

    def load_next(self):
        return self.oai.load_next()

    def format_affiliation(self, affiliation: str) -> Optional[dict]:
        return {'type': 'Organization', 'name': self.normalize_organizations([affiliation])[0]} if affiliation else None

    def format_name(self, *args: str) -> str:
        if len(args) == 2:
            return args[0].strip() + ' ' + args[1].strip()
        elif ',' in args[0]:
            name = args[0].split(',')
            return name[1].strip() + ' ' + name[0].strip()
        return args[0].strip()

    def format_id(self, identifier: str) -> Optional[str]:
        orcid_regex = r'\d{4}-\d{4}-\d{4}-\d{3}[0-9X]$'
        if identifier:
            if re.match(orcid_regex, identifier) and not identifier.startswith('http'):
                return 'https://orcid.org/' + identifier.strip()
            return None
        return None

    def format_publisher(self, publisher: str) -> dict:
        if self.is_organization(publisher):
            return {'type': 'Organization', 'name': publisher}
        return {'type': 'Person', 'name': publisher}

    def format_date(self, date: str) -> Optional[str]:
        if date and '-' in date:
            date = date[0:4]
        if date and date.isnumeric():
            return date + '-01-01'
        return None

    def is_organization(self, unknown_name: str) -> bool:
        """
        Determines whether a name is likely to represent an organization or not, using a predefined list of indicator words
        :param unknown_name: the name to be evaluated
        :return: Boolean for whether the name is an organization or not
        """
        orgs = [
            'university', 'universität', 'université', 'universitat', 'institute', 'institut', 'institutes',
            'library', 'bibliothek', 'bibliothèque', 'museum', 'gmbh', 'center', 'centre', 'zentrum', 'stiftung'
        ]
        return bool([o for o in orgs if o in unknown_name.lower()])

    def get_authors(self, xml_list: list[ET.Element], author_type: str) -> list[dict]:
        """
        Extracts author (creator or contributor) information from a list of XML Elements
        and formats a list of dicts according to search index schema
        :param xml_list: list of XML Elements, with five optional children
        :return: list of author dicts formatted for search index sschema
        """
        authors = []
        for author in xml_list:
            author_info = {'given_name': author.findtext("radar-elements:givenName", None, self.namespaces),
                            'family_name': author.findtext("radar-elements:familyName", None, self.namespaces),
                            'full_name': author.findtext(f"radar-elements:{author_type}Name", None, self.namespaces),
                            'affiliation': self.format_affiliation(author.findtext(f"radar-elements:{author_type}Affiliation",
                                                                              None, self.namespaces)),
                            'id': self.format_id(author.findtext("radar-elements:nameIdentifier", None, self.namespaces))}
            if author_info.get('given_name') and author_info.get('family_name'):
                author_info['name'] = self.format_name(author_info['given_name'], author_info['family_name'])
                author_info['type'] = 'Person'
                # assign other name keys to None so they will be filtered out
                author_info.update(dict.fromkeys(['given_name', 'family_name', 'full_name'], None))
            if author_info.get('full_name'):
                if self.is_organization(author_info['full_name']):
                    author_info['name'] = self.normalize_organizations([author_info['full_name']])[0]
                    author_info['type'] = 'Organization'
                else:
                    author_info['name'] = self.format_name(author_info['full_name'])
                    author_info['type'] = 'Person'
            # assign other name keys to None so they will be filtered out
            author_info.update(dict.fromkeys(['given_name', 'family_name', 'full_name'], None))
            authors.append({k: v for k, v in author_info.items() if v is not None})
        return authors

    def clean_organization_name(self, organization_name: str) -> str:
        """
        Removes parentheses and everything after, and trailing punctuation and whitespace.
        :param organization_name: the name to be cleaned
        :return: the clean name
        """
        # remove any text inside parentheses and everything after
        if '(' in organization_name:
            organization_name = organization_name.replace('(', '*(').replace(')', ')*')
            organization_name_parts = organization_name.split('*')
            organization_name = []
            for part in organization_name_parts:
                if part and part[0] != '(':
                    organization_name.append(part)
                else:
                    break
            organization_name = ''.join(organization_name)
        # remove any trailing punctuation
        organization_name = organization_name.strip(".,!?:;\'\"-")
        # remove any leading or trailing whitespaces
        organization_name = organization_name.strip()
        return organization_name

    def normalize_organizations(self, organizations: list[str]) -> list[str]:
        """
        Normalizes and cleans organization names.
        Removes parentheses and everything after, and trailing punctuation and whitespace.
        Organizations are added to the organization_map on a first-come, first-serve basis (no quality hierarchy).
        :param organizations: the organization names to be normalized
        :return: the normalized organization names
        """
        for idx in range(len(organizations)):
            found_in_map = False
            name = organizations[idx]
            # clean it
            name = self.clean_organization_name(name)
            # check if any of the keys are in the name
            for key, value in self.organizations_map.items():
                if key in name:
                    # replace the name with the one found in the map
                    organizations[idx] = key
                    found_in_map = True
                    break
            if not found_in_map:
                # add name to the map
                self.organizations_map[name] = name
                organizations[idx] = name
        return organizations

    def get_description(self, descriptions: list[ET.Element]) -> str:
        """
        Selects the most suitable description text from a list of XML Elements based on the type of the text,
        following a predefined type precedence.
        :param descriptions: list of XML Elements, each with an attribute defining the type of description
        :return: a single description text
        """
        # precedence hierarchy for description types
        type_precedence = {
            'Abstract': 1,
            'TechnicalRemarks': 2,
            'TechnicalInfo': 3,
            'Method': 4,
            'TableOfContents': 5,
            'Object': 6,
            'Other': 7
        }
        descriptions_dict = [{'type': d.attrib['descriptionType'], 'text': d.text} for d in descriptions]
        if len(descriptions_dict) == 1:
            return descriptions_dict[0]['text'].strip()
        # find description text with the greatest type precedence ranking
        return min(descriptions_dict, key = lambda d: type_precedence.get(d['type'], len(type_precedence) + 1))['text'].strip()

    def get_related_identifiers(self, related_identifiers: list[ET.Element]) -> dict:
        """
        Extracts related identifier information from a list of XML elements, according to relation type.
        :param related_identifiers: list of XML elements, each with attribute relationType and doi as text value
        :return: dict containing relation type as key and list of {id: doi} as value
        """
        part_of = [i.text for i in related_identifiers if i.attrib['relationType'] == 'IsPartOf']
        has_part = [i.text for i in related_identifiers if i.attrib['relationType'] == 'HasPart']
        part_of_obj = [{'id': 'https://doi.org/' + i.strip()} for i in part_of] if part_of else None
        has_part_obj = [{'id': 'https://doi.org/' + i.strip()} for i in has_part] if has_part else None
        return {'part_of': part_of_obj, 'has_part': has_part_obj}

    def to_search_index_metadata(self, record) -> dict:
        identifier: str = record.findtext("oai:metadata/radar-dataset:radarDataset/radar-elements:identifier", None, self.namespaces)
        identifier = 'https://doi.org/' + identifier if 'RADAR' not in identifier else identifier
        creators_xml = record.find("oai:metadata/radar-dataset:radarDataset/radar-elements:creators", self.namespaces)
        creator_xml_list = creators_xml.findall("radar-elements:creator", self.namespaces)
        creators = self.get_authors(creator_xml_list, 'creator') if creator_xml_list else [ ]
        contributors_xml = record.find("oai:metadata/radar-dataset:radarDataset/radar-elements:contributors", self.namespaces)
        contributors = [ ]
        if contributors_xml:
            contributor_xml_list = contributors_xml.findall("radar-elements:contributor", self.namespaces)
            contributors = self.get_authors(contributor_xml_list, 'contributor') if contributor_xml_list else [ ]
        title: str = record.findtext("oai:metadata/radar-dataset:radarDataset/radar-elements:title", None, self.namespaces)
        descriptions = record.findall("oai:metadata/radar-dataset:radarDataset/radar-elements:descriptions/radar-elements:description", self.namespaces)
        description = self.get_description(descriptions) if descriptions else ''
        publishers_xml = record.findall("oai:metadata/radar-dataset:radarDataset/radar-elements:publishers/radar-elements:publisher", self.namespaces)
        publishers = [p.text for p in publishers_xml] if publishers_xml else [ ]
        publishers = self.normalize_organizations(publishers)
        production_year_str: str = record.findtext("oai:metadata/radar-dataset:radarDataset/radar-elements:productionYear", None, self.namespaces)
        production_year = self.format_date(production_year_str)
        publication_year_str: str = record.findtext("oai:metadata/radar-dataset:radarDataset/radar-elements:publicationYear", None, self.namespaces)
        publication_year = self.format_date(publication_year_str)
        language_code: str = record.findtext("oai:metadata/radar-dataset:radarDataset/radar-elements:language", None, self.namespaces)
        language = [lang_mapping.get(language_code)] if language_code else None
        subject_xml = record.find("oai:metadata/radar-dataset:radarDataset/radar-elements:subjectAreas", self.namespaces)
        subject_xml_list = subject_xml.findall("radar-elements:subjectArea/radar-elements:controlledSubjectAreaName", self.namespaces)
        subjects = [self.mapping.get('subject', s.text) for s in subject_xml_list]
        lrt: str = record.find("oai:metadata/radar-dataset:radarDataset/radar-elements:resource", self.namespaces).attrib['resourceType']
        keywords_xml = record.findall("oai:metadata/radar-dataset:radarDataset/radar-elements:keywords/radar-elements:keyword", self.namespaces)
        keywords = [k.text for k in keywords_xml]
        license_name: str = record.findtext("oai:metadata/radar-dataset:radarDataset/radar-elements:rights/radar-elements:controlledRights",
                                               None, self.namespaces)
        license_uri = self.mapping.get('license', license_name) if license_name else None
        related_identifiers_xml = record.findall("oai:metadata/radar-dataset:radarDataset/radar-elements:relatedIdentifiers/radar-elements:relatedIdentifier", self.namespaces)
        related_identifiers: dict = self.get_related_identifiers(related_identifiers_xml)

        meta = {
            "@context": [
                "https://schema.org", {"@language": "en"}
            ],
            "type": [self.mapping.get("resourceType", lrt)] if lrt else ["Dataset"],
            "id": identifier,
            "name": title,
            "description": description,
            "inLanguage": language,
            "license": {"id": license_uri} if license_uri else None,
            "creator": creators,
            "contributor": contributors,
            "about": [{'id': s} for s in subjects if s is not None] if subjects else None,
            "keywords": keywords,
            "dateCreated": production_year,
            "datePublished": publication_year,
            "publisher": [self.format_publisher(p) for p in publishers if p is not None] if publishers else None,
            "isPartOf": related_identifiers.get('part_of'),
            "hasPart": related_identifiers.get('has_part'),
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://www.radar-service.eu"
                    }
                }
            ],
            "conditionsOfAccess": {"id": "http://w3id.org/kim/conditionsOfAccess/no_login"}
        }
        return meta

if __name__ == "__main__":
    RADARImport().process()
