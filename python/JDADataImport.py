from JDADataMapping import JDADataMapping
from CKANHelpers import CKANImportBase
import re
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject

unicode_dict = {"o\\u0308": "ö", "\\u00f6": "ö", "\\u00e1": "á", "\\u00f8": "ø", "\\u00fc": "ü", "\\u00e4": "ä", "\\u00df": "ß", "\\u00e6": "æ", "\\u00d8": "Ø", "\\u00e9": "é", "\\u0301": "ń", "\\u0307": "ż", "\\u00f3": "ó", '"':"", "n\u0301": "ń", "z\u0307": "ż", "o\u0308":"ö", "\\u00ed":"í", "'":""}

def built_creator(item):
    creator_dict = {"type": "Person"}
    pattern_lastname = r'(?<="lastname": ").[^"]+'
    pattern_firstname = r'(?<="firstname": ").[^"]+'
    firstname = re.findall(pattern_firstname, item)
    lastname = re.findall(pattern_lastname, item)
    replacement = dict((re.escape(k), v) for k, v in unicode_dict.items())
    pattern = re.compile("|".join(replacement.keys()))
    lastname = pattern.sub(lambda m: replacement[re.escape(m.group(0))], lastname[0])
    firstname = pattern.sub(lambda m: replacement[re.escape(m.group(0))], firstname[0])
    creator_name = firstname + " " + lastname if len(firstname) >0 else lastname.lstrip(", ")
    creator_dict.update({"name": creator_name.lstrip(", ")})
    return creator_dict

class JDADataImport(CKANImportBase):
    def __init__(self):
        super().__init__("ZBW Journal Data Archive", "journaldata.zbw.eu", mapping=JDADataMapping())

    def to_search_index_metadata(self, record):
        if record.get("owner_org") == "12c8a376-9e8e-41ba-97d1-093d63d2abd0":
            return None
        meta = self.map_default(record)
        jels = record.get("dara_jels")
        if isinstance(jels, list):
            jel = list(set(map(lambda j: self.mapping.get("jel", j[0]), jels)))
            meta["about"] = list(map(lambda j: {"id": j}, jel))
        else:
            meta["about"] = [{"id": Subject.N30_BUSINESS_AND_ECONOMICS}]

        meta["sourceOrganization"] = [{"type": "Organization", "name": "ZBW", "id": "https://ror.org/03a96gc12"}]
        res_type = [self.mapping.get("resourceType", record["type"])]
        if meta["encoding"]:
            for i in range(len(meta["encoding"])):
                if meta["encoding"][i]["encodingFormat"] is not None:
                    if re.match("R|STATA.+|SPS|SRC|LOG", meta["encoding"][i]["encodingFormat"]):
                        meta["encoding"][i].pop("encodingFormat")
                if not re.match("^\d+$", meta["encoding"][i]["contentSize"]):
                    meta["encoding"][i].pop("contentSize")

        publisher = record.get("owner_org")
        meta["publisher"] = list(filter(None, map(lambda p: {"type": "Organization", "name": self.mapping.get("owner_org", p)}, filter(None, [publisher]))))

        creator_string = record.get("dara_authors")
        creator_list = creator_string.split("}, {") if creator_string is not None else None

        meta["@context"].append({"@language": "en"})
        meta["inLanguage"] = ["en"]
        meta["datePublished"] = (record.get("dara_PublicationDate") + "-01-01")
        meta["name"] = record["title"]
        meta["type"] = res_type if res_type else ["Dataset"]
        meta["license"] = {"id": "https://creativecommons.org/licenses/by/4.0/"}
        meta["creator"] = list(map(lambda c: built_creator(c), creator_list)) if creator_list is not None else None
        return meta


if __name__ == "__main__":
    JDADataImport().process()