from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class GitMetadataMapping(SearchIndexImportMapping):
    def __init__(self):
        self.add_mapping("lrt", {
            "Kurs": "Course",
            "Course": "Course",
            "https://w3id.org/kim/hcrt/application": "SoftwareApplication",
            "https://w3id.org/kim/hcrt/audio": "AudioObject",
            "https://w3id.org/kim/hcrt/course": "Course",
            "https://w3id.org/kim/hcrt/data": "Dataset",

            "https://w3id.org/kim/hcrt/image": "ImageObject"

        }, None)

