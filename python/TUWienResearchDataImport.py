import requests
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.mapping.iso639_2_to_iso639_1 import mapping as lang_mapping
from TUWienResearchDataMapping import TUWienResearchDataMapping
from bs4 import BeautifulSoup
from typing import Optional
import re

license_links = ['https://creativecommons.org/licenses/by/4.0/']
orgname_normalization = r'(?i)\b(TU Wien|Technische Universität Wien)\b'

class TUWienResearchDataImport(ResodateImporter):

    domain = "https://researchdata.tuwien.at/api/records"
    items_per_request = 25

    def __init__(self):
        super().__init__(mapping=TUWienResearchDataMapping())
        self.page = 1

    def get_name(self):
        return "TU Wien Research Data"

    def load_next(self) -> Optional[list[dict]]:
        headers = {"Content-Type": "application/json;charset=UTF-8", "Accept": "application/json", "User-Agent": self.user_agent}
        params = {"size": self.items_per_request, "page": self.page}
        resp = requests.get(self.domain, params=params, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + self.get_name() + ": {}".format(resp))
        self.page += 1
        json = resp.json()
        if not json["hits"]["hits"]:
            return None
        records = json["hits"]["hits"]
        # additional API call to fetch encodings data
        for record in records:
            access = record['access'].get('status')
            if access == 'open' or access is None:
                files_link = record['links'].get('files')
                if files_link:
                    record["encoding"] = self.get_encodings(files_link)
        return records

    def get_encodings(self, link: str) -> list[dict]:
        """
        Record metadata includes a link to the metadata of all associated files.
        This method makes a GET request to fetch the files' metadata and extract to the RESODATE encoding schema.
        :param link: the API link to a record's files' metadata
        :return: list of file metadata objects
        """
        encodings = [ ]
        headers = {"Content-Type": "application/json;charset=UTF-8", "Accept": "application/json", "User-Agent": self.user_agent}
        resp = requests.get(link, headers=headers)
        # returns 403 if the files are not publicly available
        if resp.status_code != 200:
            return [ ]
        files_info = resp.json().get('entries')
        if not files_info:
            return [ ]
        for file in files_info:
            info = {"type": "MediaObject",
                    "contentSize": str(file.get('size')) if file.get('size') else None,
                    "encodingFormat": file.get('mimetype'),
                    "contentUrl": file['links'].get('content')}
            encodings.append(info)
        return encodings

    def get_license(self, rights: list) -> Optional[dict]:
        if rights[0].get('props'):
            return {"id": rights[0]['props']['url']}
        elif rights[0].get('link') in license_links:
            return {"id": rights[0]['link']}
        else:
            return None

    def get_creator_name_and_type(self, individual: dict) -> dict:
        creator = { }
        creator['type'] = self.mapping.get('creatorType', individual['person_or_org'].get('type'))
        given_name = individual['person_or_org'].get('given_name')
        family_name = individual['person_or_org'].get('family_name')
        full_name = individual['person_or_org'].get('name')
        if type(given_name) is str and type(family_name) is str:
            creator['name'] = given_name + ' ' + family_name
        elif creator['type'] == 'Organization':
            creator['name'] = full_name
        elif full_name and ',' in full_name:
            name = full_name.split(',')
            creator['name'] = name[1] + ' ' + name[0]
        return creator

    def get_creators_and_organizations(self, creator_info: list[dict]) -> tuple[list, list]:
        creators = [ ]
        organizations = [ ]
        for individual in creator_info:
            creator = self.get_creator_name_and_type(individual)
            if individual['person_or_org'].get('identifiers'):
                scheme = individual['person_or_org']['identifiers'][0]['scheme']
                identifier = self.mapping.get('identifier', scheme) + individual['person_or_org']['identifiers'][0]['identifier']
                creator['id'] = identifier
            if individual.get('affiliations'):
                if individual['affiliations'][0].get('name'):
                    name = individual['affiliations'][0]['name']
                    if re.search(orgname_normalization, name):
                        name = 'TU Wien'
                    affiliation = {'type': 'Organization', 'name': name}
                    creator['affiliation'] = affiliation
                    organizations.append(affiliation)
            creators.append(creator)
        # remove duplicate creators/organizations
        creators_dict = {c['name']: c for c in creators}
        distinct_creators = list(creators_dict.values())
        organizations_dict = {o['name']: o for o in organizations}
        distinct_organizations = list(organizations_dict.values())
        return distinct_creators, distinct_organizations

    def to_search_index_metadata(self, record: dict) -> dict:
        access = record['access'].get('status')
        if access and access != 'open':
            return { }
        record_metadata = record['metadata']
        resource_type = record_metadata.get('resource_type').get('id') if record_metadata.get('resource_type') else None
        license_id = self.get_license(record_metadata.get('rights')) if record_metadata.get('rights') else None
        publisher = record_metadata.get("publisher")
        creators, organizations = (self.get_creators_and_organizations(record_metadata.get('creators'))
                                  if record_metadata.get('creators') else None)
        id_ = "https://researchdata.tuwien.at/records/" + record["id"]
        keywords = [d['subject'] for d in record_metadata.get('subjects')] if record_metadata.get('subjects') else [ ]
        publication_date = record_metadata.get('publication_date')
        description = record_metadata.get('description')
        if description:
            soup = BeautifulSoup(description, 'html.parser')
            description = soup.get_text()
        language = ([lang_mapping.get(lang['id']) for lang in record_metadata.get('languages')]
                    if record_metadata.get('languages') else [])
        meta = {
            "@context": [
                "https://schema.org", {"@language": "en"}
            ],
            "type": [self.mapping.get('resourceType', resource_type)] if resource_type else None,
            "id": id_,
            "name": record_metadata.get('title'),
            "description": description,
            "license": license_id,
            "inLanguage": [lang for lang in language if lang is not None],
            "publisher": [{"type": "Organization", "name": publisher}] if publisher else None,
            "creator": creators,
            "sourceOrganization": organizations,
            "keywords": keywords,
            "datePublished": publication_date if len(publication_date) == 10 else None,
            "conditionsOfAccess": {"id": 'https://w3id.org/kim/conditionsOfAccess/no_login'},
            "encoding": record.get("encoding"),
            "mainEntityOfPage": [
                {
                    "id": id_,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": id_
                    },
                    "dateCreated": record["created"][:10],
                    "dateModified": record["updated"][:10]
                }
            ]
        }
        return meta


if __name__ == "__main__":
    TUWienResearchDataImport().process()
