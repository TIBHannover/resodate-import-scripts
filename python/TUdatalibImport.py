import re

from TUdatalibMapping import TUdatalibMapping
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader


class TUdatalibImport(ResodateImporter):
    def __init__(self):
        super().__init__(mapping=TUdatalibMapping())
        self.loaded = False
        self.oai = OaiPmhReader("https://tudatalib.ulb.tu-darmstadt.de/oai/request", "xoai", None, self.user_agent)

    def get_name(self):
        return "TUdatalib"

    def load_next(self):
        return self.oai.load_next()

    def get_language_code(self, language):
        language_code = re.sub("_.*", "", language) if language else None
        if language_code == 'other':
            language_code = None
        return language_code

    def check_uri_validity(self, uri):
        if uri and 'http' not in uri:
            uri = None
        return uri

    def to_search_index_metadata(self, record):
        namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/", "xoai": "http://www.lyncode.com/xoai"}
        record_status = record.find("oai:header", namespaces)
        if "status" in record_status.attrib and record_status.get("status") == "deleted":
            return None
        dc_xml = record.find("oai:metadata/xoai:metadata/xoai:element[@name='dc']", namespaces)
        keywords = list(set(map(lambda s: s.text.strip(), dc_xml.findall("xoai:element[@name='subject']/xoai:element/xoai:field", namespaces))))
        subjects = list(set(filter(None, map(lambda s: self.mapping.get("subject", s.text.strip()), dc_xml.findall("xoai:element[@name='subject']/xoai:element[@name='classification']/xoai:element/xoai:field", namespaces)))))
        language = dc_xml.findtext("xoai:element[@name='language']/xoai:element/xoai:element/xoai:field", None, namespaces)
        language_code = self.get_language_code(language)
        description = dc_xml.findtext("xoai:element[@name='description']/xoai:element[@name='de_DE']/xoai:field", None, namespaces)
        license_uri = dc_xml.findtext("xoai:element[@name='rights']/xoai:element[@name='uri']/xoai:element/xoai:field", None, namespaces)
        license_uri = self.check_uri_validity(license_uri)
        creator_xml = dc_xml.findall("xoai:element[@name='contributor']/xoai:element[@name='author']/xoai:element/xoai:field[@name='value']", namespaces)
        contributor_xml = dc_xml.findall("xoai:element[@name='contributor']/xoai:element[@name='advisor']/xoai:element/xoai:field", namespaces)
        publisher_xml = dc_xml.findall("xoai:element[@name='publisher']/xoai:element/xoai:field", namespaces)
        lrts = list(set(map(lambda t: self.mapping.get("lrt", t.text.strip()),dc_xml.findall("xoai:element[@name='type']/xoai:element/xoai:field", namespaces))))
        identifier = dc_xml.findtext("xoai:element[@name='identifier']/xoai:element[@name='uri']/xoai:element/xoai:field", None, namespaces)
        bundles_xml = list(filter(lambda e: "ORIGINAL" == e.findtext("xoai:field", None, namespaces), record.findall("oai:metadata/xoai:metadata/xoai:element[@name='bundles']/xoai:element[@name='bundle']", namespaces)))
        thumbnails_xml = list(filter(lambda e: "THUMBNAIL" == e.findtext("xoai:field", None, namespaces), record.findall("oai:metadata/xoai:metadata/xoai:element[@name='bundles']/xoai:element[@name='bundle']", namespaces)))
        image = thumbnails_xml[0].findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='url']", None, namespaces) if thumbnails_xml else None
        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "type": lrts if lrts and lrts != [None] else ["Dataset"],
            "id": identifier if identifier else None,
            "name": dc_xml.findtext("xoai:element[@name='title']/xoai:element/xoai:field", None, namespaces),
            "image": image,
            "description": description.strip() if description else None,
            "inLanguage": [language_code] if language_code else None,
            "license": {"id": license_uri} if license_uri else None,
            "creator": list(filter(None, map(lambda s: {"type": "Person", "name": s.text.strip()} if s.text else None, creator_xml))) if creator_xml else None,
            "contributor": list(filter(None, map(lambda s: {"type": "Person", "name": s.text.strip()} if s.text else None, contributor_xml))) if contributor_xml else None,
            "about": list(map(lambda s: {"id": s}, subjects)),
            "keywords": keywords,
            "sourceOrganization": [{"type": "Organization", "name": "Technische Universität Darmstadt", "id": "https://ror.org/05n911h24"}],
            # "dateCreated": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='issued']/xoai:element/xoai:field", None, namespaces),
            "datePublished": dc_xml.findtext("xoai:element[@name='date']/xoai:element[@name='accessioned']/xoai:element/xoai:field", None, namespaces),
            "publisher": list(map(lambda s: {"type": "Organization", "name": s.text.strip()}, publisher_xml)),
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://tudatalib.ulb.tu-darmstadt.de"
                    }
                }
            ],
            "encoding": list(map(lambda e: {
                "contentUrl": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='url']", None, namespaces),
                "encodingFormat": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='format']", None, namespaces),
                "contentSize": e.findtext("xoai:element[@name='bitstreams']/xoai:element[@name='bitstream']/xoai:field[@name='size']", None, namespaces)
            }, bundles_xml)),
            "conditionsOfAccess": {"id": "http://w3id.org/kim/conditionsOfAccess/no_login"}
        }
        return meta


if __name__ == "__main__":
    TUdatalibImport().process()
