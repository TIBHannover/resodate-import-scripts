from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class ORKGMapping(SearchIndexImportMapping):

    def __init__(self):

        self.add_mapping("license", {
            "Apache License": "https://www.apache.org/licenses/LICENSE-2.0",
            "Apache": "https://www.apache.org/licenses/LICENSE-2.0",
            "Artistic License": None,
            "BSD license": "https://www.opensource.org/licenses/BSD-3-Clause",
            "GNU GENERAL PUBLIC LICENSE": "https://www.gnu.org/licenses/gpl-3.0",
            "GNU GPL": "https://www.gnu.org/licenses/gpl-3.0",
            "GNU General Public Licence": "https://www.gnu.org/licenses/gpl-3.0",
            "GNU General Public License": "https://www.gnu.org/licenses/gpl-3.0",
            "GNU LGPL": "https://www.gnu.org/licenses/lgpl-3.0",
            "GNU general public license": "https://www.gnu.org/licenses/gpl-3.0",
            "GPL": "https://www.gnu.org/licenses/gpl-3.0",
            "MIT License": "https://opensource.org/licenses/MIT",
            "MIT license": "https://opensource.org/licenses/MIT",
            "http://orkg.org/orkg/resource/R213553": "https://www.apache.org/licenses/LICENSE-2.0",
            "http://orkg.org/orkg/resource/R213583": "https://www.gnu.org/licenses/gpl-3.0",
            "http://orkg.org/orkg/resource/R226918": "https://opensource.org/licenses/MIT",
            "http://orkg.org/orkg/resource/R227481": "https://www.opensource.org/licenses/BSD-3-Clause",
        }, None)
