import re

from CKANHelpers import CKANImportBase
from LUHForschungsdatenMapping import LUHForschungsdatenMapping
from search_index_import_commons.Helpers import get, parse_entity_name


class LUHForschungsdatenImport(CKANImportBase):

    def __init__(self):
        super().__init__("Forschungsdaten Universität Hannover", "data.uni-hannover.de", mapping=LUHForschungsdatenMapping())
        self.record_count = 0

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        lrts = [self.mapping.get("lrt", record["type"])]
        creators = re.split(" and |, ?|; ?| & ", get(record, "author"))
        creators = list(filter(lambda a: re.match(r"([a-zA-Z].*){2,} ([a-zA-Z].*){2,}", a), creators))
        subject = self.mapping.get("orga_to_subject", record["organization"]["title"].strip())
        meta["type"] = lrts if lrts else ["Dataset"]
        meta["creator"] = list(map(lambda c: parse_entity_name(c.replace("et al.", "").strip()), creators))
        meta["image"] = "https://data.uni-hannover.de/base/images/ckan-logo.png"
        meta["about"] = [{"id": subject}] if subject else []
        meta["license"] = {"id": record["license_url"]} if get(record, "license_url") else None
        meta["sourceOrganization"] = list(map(lambda r: {"name": r["title"], "type": "Organization"}, [record["organization"]]))
        meta["conditionsOfAccess"] = {"id": "http://w3id.org/kim/conditionsOfAccess/no_login"}
        return meta


if __name__ == "__main__":
    LUHForschungsdatenImport().process()
