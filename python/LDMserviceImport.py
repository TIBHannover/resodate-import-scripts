import re
from CKANHelpers import CKANImportBase
from LDMserviceMapping import LDMMapping
from search_index_import_commons.Helpers import get, parse_entity_name

def get_creator(author_list):
    return author_list["extra_author"]

def normalise_creators(record):
    if re.match(r"^([^,]+, ){2,}", get(record, "author")):
        creators = re.split(", ", get(record, "author"))
    else:
        creators = re.split(",? and |; ?| & ", get(record, "author"))

    if "extra_authors" in record:
        extra_authors = list(filter(None, map(get_creator, record["extra_authors"])))
        creators = creators + extra_authors

    if record["organization"]["title"] == "PANGAEA (Agriculture)":
        normalised_creators = list(map(lambda a: re.sub(r"(^[^ ]+)( )(.*)", r"\3 \1", a), creators))
    else:
        normalised_creators = list(map(lambda n: re.sub(r"(^[^,]+)(, )(.*)", r"\3 \1", n), creators))
    return normalised_creators

def build_creator(normalised_creators):
    academic_degrees = re.compile(r'Prof\.|Dr\.-Ing\.|Dr\.|habil\.')
    creators_pers = list(filter(lambda a: not re.match(r".*([mM]useum|[sS]tiftung|[Cc]onsortium|[iI]nstitute?|[aA]ffairs|[Uu]niversit|[Bb]ibliothek|ZAK|digitus).*", a), normalised_creators))
    if len(creators_pers)>0:
        creator = list(map(lambda a: {"name": re.sub(academic_degrees,"", a).strip(), "type": "Person"}, creators_pers))
    else:
        creators_organization = list(filter(lambda a: re.match(r".*([mM]useum|[sS]tiftung|[Cc]onsortium|[iI]nstitute?|[aA]ffairs|[Uu]niversit|[Bb]ibliothek|ZAK|digitus).*", a), normalised_creators))
        creator = list(map(lambda a: {"name": a.strip(), "type": "Organization"}, creators_organization)) if creators_organization else None
    return creator

def url_clean_up(url):

    if ", " in url:
        urls = url.split(",")
        urls = [i for i in urls if "doi" in i]
        url = urls[0]

    return url

class LDMserviceImport(CKANImportBase):

    def __init__(self):
        super().__init__("Leibniz Data Manager", "service.tib.eu/ldmservice", mapping=LDMMapping())
        self.record_count = 0

    def to_search_index_metadata(self, record):

        meta = self.map_default(record)
        resource_type = [self.mapping.get("type", record["type"])]
        meta["@context"].append({"@language": "en"})

        source = record.get("url")
        if source:
            source_url = url_clean_up(source)
            meta["id"] = source_url.strip()

        if "subject_areas" in record:
            subject_area = list(filter(lambda t: "Other" not in t, [s["subject_area_name"] for s in record["subject_areas"]]))
            meta["keywords"] = meta["keywords"] + subject_area if meta["keywords"] else subject_area
            subjects = list(set(map(lambda s: self.mapping.get("subject_area", s), subject_area)))
            meta["about"] = list(map(lambda s: {"id": s}, subjects))
        else:
            subject = self.mapping.get("orga_to_subject", record["organization"]["title"].strip())
            meta["about"] = [{"id": subject}] if subject else []

        license_uri = self.mapping.get("license", get(record, "license_id"))

        if "publishers" in record:
            meta["publisher"] = list(map(lambda p: {"type": "Organization", "name": p["publisher"]}, get(record, "publishers")))
        meta["type"] = resource_type if resource_type else ["Dataset"]
        creator = normalise_creators(record)
        meta["creator"] = build_creator(creator)
        meta["image"] = "https://projects.tib.eu/fileadmin/templates/datamanager/tib_datamanager_1150.jpg"
        meta["license"] = {"id": license_uri} if license_uri else None
        meta["datePublished"] = get(record, "publication_year") + "-01-01" if get(record, "publication_year") else None
        return meta


if __name__ == "__main__":
    LDMserviceImport().process()