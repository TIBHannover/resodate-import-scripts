import re
from FordatisMapping import FordatisMapping
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.OaiPmhReader import OaiPmhReader


def build_source_organization(self, record, namespaces):
    source_organizations = [x for x in record.findall("oai:header/oai:setSpec", namespaces) if x.text != "openaire_data"]
    source_organizations_list = []
    for source_organisation in source_organizations:
        source_organization_name = list(filter(None, map(lambda o: self.mapping.get("institut", o.text.strip()), [source_organisation])))
        source_organization_ror = list(map(lambda o: self.mapping.get("ror", o.strip()), source_organization_name))
        source_organizations_list.append({"type": "Organization", "name": source_organization_name[0], "id": source_organization_ror[0]})
    return source_organizations_list

def build_date(date):
    full_pattern = re.compile(r"\d{4}-\d{2}-\d{2}")
    yy_mm_pattern = re.compile(r"\d{4}-\d{2}")
    if re.match(full_pattern, date) is not None:
        date = date
    elif re.match(yy_mm_pattern, date) is not None:
        date = date + "-01"
    else:
        date = date + "-01-01"
    return date

def build_pers_names(name):
    contributor_dict = {"type": "Person"}
    contributor_name = name.text
    contributor_name = re.sub(r"^([^,]+), (.+$)", r"\2 \1", contributor_name)
    contributor_dict.update({"name": contributor_name})
    return contributor_dict

def build_description(record, namespaces):
    description_abstract = " ".join(list(map(lambda d: d.text.strip().replace(r"\s+", " ").replace(r"\r\n", r"\n"), record.findall("oai:metadata/fordatis-all:fordatis/dcterms:descriptionAbstract", namespaces))))
    description_sponsorship = " ".join(list(map(lambda d: d.text.strip().replace(r"\s+", " ").replace(r"\r\n", r"\n"), record.findall("oai:metadata/fordatis-all:fordatis/dcterms:descriptionSponsorship", namespaces))))
    description_techinfo = " ".join(list(map(lambda d: d.text.strip().replace(r"\s+", " ").replace(r"\r\n", r"\n"), record.findall("oai:metadata/fordatis-all:fordatis/dcterms:descriptionTechnicalinformation", namespaces))))
    description = (description_abstract if description_abstract else "") + ("\nSponsor: " + description_sponsorship if description_sponsorship else "") + ("\nTechnical Information: " + description_techinfo if description_techinfo else "")
    return description

class FordatisImport(ResodateImporter):
    def __init__(self):
        super().__init__(mapping=FordatisMapping())
        self.loaded = False
        self.oai = OaiPmhReader("https://fordatis.fraunhofer.de/oai/request", "fordatis", None, self.user_agent)

    def get_name(self):
        return "fordatis"

    def load_next(self):
        return self.oai.load_next()

    def to_search_index_metadata(self, record):
        namespaces = {"oai": "http://www.openarchives.org/OAI/2.0/", "fordatis": "http://fordatis.fraunhofer.de/namespace/metadata/schema" , "fordatis-all": "http://fordatis.fraunhofer.de/namespace/metadata/schema-all", "dcterms": "http://dublincore.org/documents/dcmi-terms/"}
        record_status = record.find("oai:header", namespaces)
        if "status" in record_status.attrib and record_status.get("status") == "deleted":
            return None

        resource_type = list(set(map(lambda t: self.mapping.get("resourcetype", t.text.strip()), record.findall("oai:metadata/fordatis-all:fordatis/dcterms:type", namespaces=namespaces))))
        identifier = record.findtext("oai:header/oai:identifier", namespaces=namespaces).replace("oai:", "").replace("https://fordatis.fraunhofer.de:fordatis/","https://fordatis.fraunhofer.de/handle/fordatis/")

        # # clean up ddc notations
        subjects_raw = record.findall("oai:metadata/fordatis-all:fordatis/dcterms:subjectDdc", namespaces=namespaces)
        subjects_splitted = [l for ls in [sub.text.replace("DDC::","").split("::") for sub in subjects_raw] for l in ls]
        notations = list(set(map(lambda n: re.sub(r'(^\d{3})(.+)', r'\g<1>', n), subjects_splitted)))
        subjects_alt = set(list(filter(None, map(lambda s: self.mapping.get("institute2subject", s.text), record.findall("oai:metadata/fordatis-all:fordatis/fordatis:institute", namespaces=namespaces)))))
        subjects = set(list(filter(None, map(lambda s: self.mapping.get("ddc_classification", s), notations)))) if notations else subjects_alt

        keywords = list(set(map(lambda s: s.text.strip(), record.findall("oai:metadata/fordatis-all:fordatis/dcterms:subject", namespaces=namespaces))))
        description = build_description(record, namespaces)
        date = record.findtext("oai:metadata/fordatis-all:fordatis/dcterms:dateIssued", namespaces=namespaces)
        dcterms_rights_uris = record.findall("oai:metadata/fordatis-all:fordatis/dcterms:rightsUri", namespaces=namespaces)
        licenses = [x for x in dcterms_rights_uris if "rightsstatements.org" not in x.text]
        usage_info  = [x for x in dcterms_rights_uris if "rightsstatements.org" in x.text]
        license_uri = list(filter(None, map(lambda l: l.text, licenses)))
        usage_info_uri = list(filter(None, map(lambda u: u.text, usage_info)))
        creators = record.findall("oai:metadata/fordatis-all:fordatis/dcterms:contributorAuthor", namespaces=namespaces)
        contributors = record.findall("oai:metadata/fordatis-all:fordatis/dcterms:contributorAdvisor", namespaces=namespaces) + record.findall("oai:metadata/fordatis-all:fordatis/dcterms:contributorOther", namespaces=namespaces)

        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "id": identifier,
            "type": resource_type if resource_type else ["Dataset"],
            "sourceOrganization": build_source_organization(self, record, namespaces),
            "name": record.findtext("oai:metadata/fordatis-all:fordatis/dcterms:title", namespaces=namespaces),
            "description": description,
            "inLanguage": list(filter(None, map(lambda l: self.mapping.get("language", l.text.strip()) if l.text else None, record.findall("oai:metadata/fordatis-all:fordatis/dcterms:languageIso", namespaces=namespaces)))),
            "license": {"id": self.mapping.get("license", license_uri[0])} if license_uri else None,
            "usageInfo": {"id": usage_info_uri[0]} if usage_info_uri else None,
            "creator": list(filter(None, map(lambda c: build_pers_names(c), creators))) if creators is not None else None,
            "contributor": list(filter(None, map(lambda c: build_pers_names(c), contributors))) if contributors is not None else None,
            "about": list(map(lambda s: {"id": s}, subjects)),
            "keywords": keywords if keywords else None,
            "datePublished": build_date(date),
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": "https://fordatis.fraunhofer.de"
                    }
                }
            ]
        }
        return meta


if __name__ == "__main__":
    FordatisImport().process()
