from common.ResodateImporter import ResodateImporter
from search_index_import_commons.mapping.iso639_2_to_iso639_1 import mapping as lang_mapping
from FintofiServiceMapping import FintofiServiceMapping
import requests
from rdflib import Graph
import json
from typing import Optional


class FintofiServiceImport(ResodateImporter):

    domain = 'https://api.finto.fi/rest/v1/'
    request_format = 'application/json'
    data_format = "application/rdf+xml"
    lang = '*'
    data_fetched = False

    def __init__(self):
        super().__init__(mapping=FintofiServiceMapping())

    def get_name(self):
        return "Finto.fi Service"

    def load_next(self) -> Optional[list[dict]]:
        if not self.data_fetched:
            # get ids of all vocabularies
            all_vocabulary_ids = self.get_vocabulary_ids()
            records = []
            for vocab in all_vocabulary_ids:
                vocab_metadata = self.process_vocabulary(vocab)
                if len(vocab_metadata) > 1:
                    records.append(vocab_metadata)
            self.data_fetched = True
            return [record for record in records if record]
        else:
            return None

    def process_vocabulary(self, vocab: dict) -> dict:
        """
        Fetches and combines metadata from all associated concept schema URIs for each vocabulary.

        :param vocab: The vocabulary to process.
        :return: A dictionary containing the combined metadata of the vocabulary.
        """
        # get all concept schema uri(s) for vocab
        concept_schemes_uris = self.get_vocabulary_concept_schema_uris(vocab['id'])
        vocab_metadata = {'vocab_id': vocab['id']}
        for uri in concept_schemes_uris:
            # fetch and combine metadata from all associated concept schema uris
            vocab_schema = self.get_concept_data(vocab['id'], uri)
            if vocab_schema:
                vocab_metadata = self.parse_schema_and_serialize_to_json(vocab_schema, vocab_metadata)
        # check if minimum required property found
        if 'http://purl.org/dc/elements/1.1/title' in vocab_metadata.keys():
            # get subject concept groups
            if "http://purl.org/dc/terms/subject" in vocab_metadata.keys():
                vocab_metadata['about'] = self.get_subject_concept_groups(
                    vocab_metadata["http://purl.org/dc/terms/subject"])
        return vocab_metadata

    def get_vocabulary_ids(self) -> list[dict]:
        """
        Fetches a list of IDs for all vocabularies contained in the source domain.

        :return: A list of dictionaries containing vocabulary IDs.
        """
        headers = {"Accept": self.request_format, "Content-Type": self.request_format, "User-Agent": self.user_agent}
        params = {"lang": self.lang}
        resp = requests.get(self.domain + "vocabularies", params=params, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + ": {}".format(resp))
        return resp.json()["vocabularies"]

    def get_vocabulary_concept_schema_uris(self, vocab_id: str) -> list[str]:
        """
        Fetches the URIs of concept schemes for a given vocabulary ID.

        :param vocab_id: The vocabulary ID to fetch concept schemes for.
        :return: A list of concept scheme URIs.
        """
        headers = {"Accept": self.request_format, "Content-Type": self.request_format, "User-Agent": self.user_agent}
        resp = requests.get(self.domain + vocab_id, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + ": {}".format(resp))
        return [c.get('uri') for c in resp.json()['conceptschemes']]

    def get_concept_data(self, vocab_id: str, uri: str) -> Optional[str]:
        """
        Fetches concept data from a given vocabulary.

        :param vocab_id: The identifier of the vocabulary.
        :param uri: The URI of the concept to retrieve.
        :return: The concept data as a string if the request is successful, otherwise None.
        """
        headers = {"Accept": self.data_format, "Content-Type": self.data_format, "User-Agent": self.user_agent}
        params = {"format": self.data_format, "uri": uri, "lang": "en"}
        resp = requests.get(self.domain + vocab_id + '/data', headers=headers, params=params)
        if resp.status_code != 200:
            return None
        return resp.text

    def parse_rdf_data(self, rdf: str) -> Graph:
        """
        Parses RDF data into an RDFLib Graph object.

        :param rdf: The RDF data as a string.
        :return: An RDFLib Graph object containing the parsed RDF data.
        """
        return Graph().parse(data=rdf, format=self.data_format)

    def parse_schema_and_serialize_to_json(self, schema: str, metadata: dict) -> dict:
        """
        Parses an RDF schema and serialize it to JSON-LD, updating the metadata dictionary.

        :param schema: The RDF schema as a string to be parsed.
        :param metadata: The dictionary to be updated with parsed schema data.
        :return: The updated metadata dictionary.
        """
        g = self.parse_rdf_data(schema)
        meta_str = g.serialize(format="json-ld")
        meta_lst = json.loads(meta_str)
        for d in meta_lst:
            for k, v in d.items():
                metadata[k] = v
        return metadata

    def get_subject_concept_groups(self, subjects: list[dict]) -> list:
        """
        Fetches concept groups for a list of subjects.

        :param subjects: A list of dictionaries, each containing subject data.
        :return: A list of concept groups' preferred labels in English.
        """
        query = """
        SELECT DISTINCT ?prop
        WHERE {
            ?subject a isothes:ConceptGroup;
               skos:prefLabel ?prop .
            FILTER(LANG(?prop) = "en")
        }
        """
        concept_groups = []
        for subject in subjects:
            rdf = self.get_concept_data('yso', subject['@id'])
            g = self.parse_rdf_data(rdf)
            concept_groups += [str(r.prop) for r in g.query(query)]
        return list(set(concept_groups))

    def get_text_by_language(self, all_terms: list[dict], language: list[str]) -> Optional[list]:
        """
        Retrieves text values from a list of terms based on a language precedence hierarchy:
        If available, return English; otherwise return the language of the resource itself.

        :param all_terms: A list of dictionaries containing term data.
        :param language: The list of languages the resource is in.
        :return: A list of text values in the specified languages if found, otherwise None.
        """
        if not all_terms:
            return None
        localized_terms = [term_dict for term_dict in all_terms if '@language' in term_dict.keys()]
        en_text = list(filter(lambda term: (term['@language'] == 'en'), localized_terms))
        if not en_text:
            for lang in language:
                lang_text = list(filter(lambda term: (term['@language'] == lang), localized_terms))
                if lang_text:
                    return [text['@value'] for text in lang_text]
        return [text['@value'] for text in en_text]

    def safe_subscripter(self, record, property_name, key_name) -> str:
        """
        Retrieves a value from a nested dictionary using given property and key names.

        :param record: The record dictionary to retrieve the value from.
        :param property_name: The name of the property to look up in the record.
        :param key_name: The key within the property to retrieve the value of.
        :return: The value associated with the key if found, otherwise None.
        """
        return (
            record.get(self.mapping.get('property', property_name))[0][key_name]
            if record.get(self.mapping.get('property', property_name))
            else None
        )

    def get_text_of_variable_format(
            self, record: dict, property_name: str, required_key: str, language: list
    ) -> Optional[list]:
        """
        Retrieves text or ID values from a specified property in a record.

        :param record: The record dictionary to retrieve the property from.
        :param property_name: The name of the property to look up in the record.
        :param required_key: The key that must be present in the property for a valid result.
        :param language: A list of preferred languages for text retrieval.
        :return: A list of text values or IDs if found, otherwise None.
        """
        property_res = record.get(self.mapping.get('property', property_name))
        if not property_res:
            return None
        if required_key not in property_res[0]:
            return None
        if '@id' in property_res[0]:
            return [p['@id'] for p in property_res]
        else:
            return self.get_text_by_language(property_res, language)

    def get_resource_language(self, record: dict) -> list:
        """
        Retrieves and maps language codes from a given record.

        :param record: The record from which to extract language information.
        :return: A list of mapped language codes.
        """
        if record.get(self.mapping.get('property', 'language')):
            languages_res: list[dict] = record.get(self.mapping.get('property', 'language'))
            languages_temp: list[str] = [lang['@id'][-3:] for lang in languages_res]
            return list(
                filter(
                    lambda lang: lang is not None,
                    [
                        lang_mapping.get(ln)
                        for ln in languages_temp
                    ] if languages_temp else []
                )
            )
        else:
            return []

    def to_search_index_metadata(self, record):
        # get language of resource in order to select text language if English not available
        language = self.get_resource_language(record)

        # record contains a list of localized text dicts
        title = self.get_text_by_language(record.get(self.mapping.get('property', 'title')), language)
        if not title:
            return {}
        creator = self.get_text_by_language(record.get(self.mapping.get('property', 'creator')), language)
        description = self.get_text_by_language(record.get(self.mapping.get('property', 'description')), language)
        publisher = self.get_text_by_language(record.get(self.mapping.get('property', 'publisher')), language)
        contributor = self.get_text_by_language(record.get(self.mapping.get('property', 'contributor')), language)

        # record contains a list of dict(s) consisting of only @id key
        license_id = self.safe_subscripter(record, 'license', '@id')
        homepage = self.safe_subscripter(record, 'homepage', '@id')

        # record contains a list of datetime dict
        date_created = self.safe_subscripter(record, 'date_created', '@value')
        date_published = self.safe_subscripter(record, 'date_published', '@value')
        date_modified = self.safe_subscripter(record, 'date_modified', '@value')

        # record contains list of either localized text dicts or @id key dicts
        part_of = self.get_text_of_variable_format(record, 'part_of', '@id', language)
        based_on = self.get_text_of_variable_format(record, 'based_on', '@value', language)

        subject = [self.mapping.get('subject', concept) for concept in record['about']] if record.get('about') else None
        finto_id = "https://finto.fi/" + record['vocab_id']

        meta = {
            "@context": [
                "https://schema.org", {"@language": "en"}
            ],
            "type": ["DefinedTermSet"],
            "id": finto_id,
            "name": ', '.join(title),
            "description": ', '.join(description) if description else None,
            "inLanguage": language,
            "creator": [{'type': 'Organization', 'name': c} for c in creator] if creator else None,
            "contributor": [{'type': 'Organization', 'name': c} for c in contributor] if contributor else None,
            "license": {'id': license_id} if license_id else None,
            "datePublished": date_published[:10] if date_published else None,
            "dateCreated": date_created[:10] if date_created else None,
            "isPartOf": [{'id': i} for i in part_of] if part_of else None,
            "isBasedOn": [{'name': n} for n in based_on] if based_on else None,
            "publisher": [{"type": "Organization", "name": p} for p in publisher] if publisher else None,
            "image": "https://www.kiwi.fi/download/attachments/42829367/Finto_fin_toissijainen_RGB_height150.png?version=1&modificationDate=1600176088137&api=v2",
            "mainEntityOfPage": [
                {
                    "id": homepage if homepage else finto_id,
                    "provider": {"type": "Service",
                                 "id": finto_id,
                                 "name": self.get_name()},
                    "dateModified": date_modified[:10] if date_modified else None
                }
            ]
        }
        if subject:
            meta['about'] = [{'id': concept} for concept in subject if concept is not None]
        return meta


if __name__ == "__main__":
    FintofiServiceImport().process()
