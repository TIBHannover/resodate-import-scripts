import re
import requests

from TerminologyServiceMapping import TerminologyServiceMapping
from common.ResodateImporter import ResodateImporter
from search_index_import_commons.Helpers import flatten, get


def get_creators(record):
    record_config = record["config"]
    creators = []
    creators.extend(record_config["creators"] if "creators" in record_config else [])
    creators.extend(record_config["annotations"]["creator"] if "annotations" in record_config and "creator" in record_config["annotations"] else [])
    creators = list(filter(lambda x: x.strip(), filter(None, creators)))
    creators = list(filter(lambda x: re.match(r"(.*[a-zA-Z]){3,}.*", x), creators))
    creators = list(filter(lambda x: not re.match(r"https?://.*", x), creators))
    creators = list(filter(lambda x: not x.startswith("mailto:"), creators))
    return list(map(lambda c: {"name": c, "type": "Person"}, set(creators)))


class TerminologyServiceImport(ResodateImporter):

    domain = "https://service.tib.eu/ts4tib"
    items_per_request = 25

    def __init__(self):
        super().__init__(mapping=TerminologyServiceMapping())
        self.page = 0

    def get_name(self):
        return "Terminology Service"

    def load_next(self):
        headers = {"Accept": "application/json", "Content-Type": "application/json", "User-Agent": self.user_agent}
        params = {"size": self.items_per_request, "page": self.page}
        resp = requests.get(self.domain + "/api/ontologies", params=params, headers=headers)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + self.get_name() + ": {}".format(resp))
        self.page += 1
        json = resp.json()
        if "_embedded" not in json:
            return None
        return json["_embedded"]["ontologies"]

    def get_subjects(self, record):
        classifications = record["config"]["classifications"]
        subject_entries = list(filter(lambda x: "Subject" in x or "subject" in x, classifications))
        subjects = flatten(list(map(lambda x: x["subject"] if "subject" in x else x["Subject"], subject_entries)))
        return list(set(filter(None, map(lambda x: self.mapping.get("subject", x), subjects))))

    def to_search_index_metadata(self, record):
        record_config = record["config"]
        license_url = record_config["license"]["url"] if "license" in record_config else None
        keywords = record_config["classifications"][0]["collection"] if "classifications" in record_config and record_config["classifications"] else None
        identifier = record_config["id"]
        if identifier.startswith('file'):
            identifier = "https://terminology.tib.eu/ts/ontologies/" + record["ontologyId"]
        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "type": ["DefinedTermSet"],
            "id": identifier,
            "name": record_config["title"],
            "creator": get_creators(record),
            "description": get(record_config, "description"),
            "license": {"id": license_url} if license_url else None,
            "about": list(map(lambda x: {"id": x}, self.get_subjects(record))),
            "keywords": keywords,
            #"sourceOrganization": list(map(lambda r: {"name": r["title"], "type": "Organization"}, [record["organization"]])),
            "conditionsOfAccess": {"id": "http://w3id.org/kim/conditionsOfAccess/no_login"},
            "mainEntityOfPage": [
                {
                    "id": "https://terminology.tib.eu/ts/ontologies/" + record["ontologyId"],
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": self.domain
                    },
                    "dateCreated": (get(record, "loaded")[:19] + "Z") if record["loaded"] else None,
                    "dateModified": (get(record, "updated")[:19] + "Z") if record["updated"] else None
                }
            ]
        }
        return meta


if __name__ == "__main__":
    TerminologyServiceImport().process()
