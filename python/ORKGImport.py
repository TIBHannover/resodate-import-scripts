import requests

from ORKGMapping import ORKGMapping
from common.ResodateImporter import ResodateImporter

query_prefixes = '''
PREFIX orkgr: <http://orkg.org/orkg/resource/>
PREFIX orkgc: <http://orkg.org/orkg/class/>
PREFIX orkgp: <http://orkg.org/orkg/predicate/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
'''


class ORKGImport(ResodateImporter):

    domain = "https://orkg.org"
    sparql_endpoint = domain + "/triplestore"
    items_per_request = 25

    def __init__(self):
        super().__init__(mapping=ORKGMapping())
        self.record_count = 0

    def get_name(self):
        return "ORKG"

    def load_next(self):

        query = '''
        %s
    
SELECT ?software, ?license, ?title, ?name, (group_concat(distinct ?keyword;separator="##+##") as ?keywords), ?description, ?url
WHERE {
  ?software a orkgc:Software.
  ?software rdfs:label ?title.
  OPTIONAL{?software orkgp:P49030 ?name.}
  OPTIONAL{?software orkgp:license ?license1.}
  OPTIONAL{?software orkgp:P45086 ?license2.}
  BIND(IF(BOUND(?license1),?license1, ?license2) AS ?license)
  OPTIONAL{?software orkgp:P3000 ?keyword.}
  OPTIONAL{?software orkgp:P14 ?description.}
  OPTIONAL{?software orkgp:P49000 ?url1.}
  OPTIONAL{?software orkgp:P45084 ?url2.}
  OPTIONAL{?software orkgp:url ?url3.}
  BIND(IF(BOUND(?url1),?url1, IF(BOUND(?url2),?url2, ?url3)) AS ?url)
  FILTER (bound(?license))
  FILTER (bound(?url))
} ORDER BY ?software
LIMIT %s
OFFSET %s
        ''' % (query_prefixes, self.items_per_request, self.record_count)
        headers = {'User-Agent': self.user_agent}
        resp = requests.get(self.sparql_endpoint, headers=headers, params={'query': query, 'format': 'json'})
        if resp.status_code != 200:
            raise IOError("cannot fetch metadata from " + self.sparql_endpoint)
        self.record_count += self.items_per_request
        result = resp.json()["results"]["bindings"]
        if len(result) == 0:
            return None
        return result

    def to_search_index_metadata(self, record):
        keywords = list(filter(None, record["keywords"]["value"].split("##+##")))
        meta = {
            "@context": [
                "https://schema.org", {"@language": "de"}
            ],
            "type": ["SoftwareApplication"],
            "id": record["url"]["value"],
            "name": record["name"]["value"] if "name" in record else record["title"]["value"],
            #"creator": list(map(lambda c: {"name": c.strip(), "type": "Person"}, creators)),
            #"image": "https://data.uni-hannover.de/base/images/ckan-logo.png",
            "description": record["description"]["value"] if "description" in record else None,
            "license": {"id": self.mapping.get("license", record["license"]["value"])},
            "keywords": keywords if keywords else None,
            #"sourceOrganization": list(map(lambda r: {"name": r["title"], "type": "Organization"}, [record["organization"]])),
            "conditionsOfAccess": {"id": "http://w3id.org/kim/conditionsOfAccess/no_login"},
            "mainEntityOfPage": [
                {
                    "id": record["software"]["value"],
                    "provider": {
                        "type": "Service",
                        "name": self.get_name(),
                        "id": self.domain
                    }
                }
            ]
        }
        return meta


if __name__ == "__main__":
    ORKGImport().process()
