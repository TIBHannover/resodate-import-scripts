from DataverseHelpers import DataverseImportBase
from search_index_import_commons.Helpers import get
from EcienciaDataMapping import EcienciaDataMapping


def _get_field(record, field_name):
    return next((s for s in record['data']["latestVersion"]["metadataBlocks"]["citation"]["fields"] if s["typeName"] == field_name), None)

def _update_metadata_language(meta, record):
    src_metadata_language = get(record["data"], "metadataLanguage")
    if src_metadata_language:
        position = meta["@context"].index({"@language": "de"})
        if src_metadata_language == "undefined":
            meta["@context"].pop(position) # will be added later in the general resodate importer
        else:
            meta["@context"][position]["@language"] = src_metadata_language

def _get_author_affiliations(author_field):
    return [author.get('authorAffiliation', {}).get('value') for author in author_field['value']
                               if author.get('authorAffiliation')]

def _clean_encoding_formats(meta):
    for encoding in meta["encoding"]:
        encoding_format = encoding.get("encodingFormat", "")
        if ";" in encoding_format:
            encoding["encodingFormat"] = encoding_format.split(";")[0]

def _clean_about_field(meta):
    meta["about"] = [entry for entry in meta["about"] if entry.get("id") is not None]
    if not meta["about"]:
        del meta["about"]


class EcienciaDataImport(DataverseImportBase):

    def __init__(self):
        super().__init__("Eciencia Data", "edatos.consorciomadrono.es", mapping=EcienciaDataMapping())

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)

        meta["image"] = "https://edatos.consorciomadrono.es/resources/images/logo-ecienciaDatosv2.png"

        src_license = get(record["data"]["latestVersion"], "license")
        if src_license:
            meta["license"] = {"id": src_license['uri']}

        src_metadata_language = get(record["data"], "metadataLanguage")
        if src_metadata_language:
            _update_metadata_language(meta, record)

        language_field = _get_field(record, "language")
        if language_field:
            mapped_languages = [
                self.mapping.get('language', language.strip())
                for language in language_field['value']
                if self.mapping.get('language', language.strip()) is not None
            ]
            if mapped_languages:
                meta["inLanguage"] = mapped_languages

        if not meta.get('about'):
            subject_field = _get_field(record, "subject")
            if subject_field:
                subjects = [self.mapping.get("subject", s.strip()) for s in subject_field["value"] if s]
                meta["about"] = [{'id': subject} for subject in subjects]

        # get author affiliations for source organization
        author_field = _get_field(record, "author")
        if author_field:
            author_affiliations = _get_author_affiliations(author_field)
            meta["sourceOrganization"] = [
                {"type": "Organization", "name": self.mapping.get("institutions", a.strip()) or a.strip()}
                for a in author_affiliations
            ] if author_affiliations else [{"type": "Organization", "name": "Eciencia Data"}]

        # clean encodingType for removing any text after a semicolon
        if "encoding" in meta:
            _clean_encoding_formats(meta)

        # confirm about key has at least one dictionary with id value
        if "about" in meta:
            _clean_about_field(meta)

        return meta


if __name__ == "__main__":
    EcienciaDataImport().process()
