### Service URL and Access Point
(How can the source be accessed? Please add API/OAI-PMH/Sitemap documentation and/or an example)

### Description
(Which source should be connected?)

### Available metadata

#### Essential data for filtering

- [ ] `subject`
- [ ] `resource type`
- [ ] `license`
- [ ] `author`
- [ ] `institution`
- [ ] `language`

#### more metadata

- [ ] `id`
- [ ] `name`
- [ ] `description`
- [ ] `datePublished`
- [ ] `encoding`
- [ ] `keywords`
- [ ] `publisher`
- [ ] `image`

/label add-source